<?php $__env->startSection('meta'); ?>
<?php if($datapc && $meta): ?>
    <title><?php echo e($datapc->title ? $datapc->title : ""); ?></title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description"
        content="<?php echo e($datapc->description ? $datapc->description : $meta->discription); ?>" />
    <meta property="og:image"
        content="<?php echo e($datapc->image != "" ? $datapc->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png'); ?>" />
    <meta name="keywords"
        content="<?php echo e($datapc->keywords ? $datapc->keywords : $meta->keysword); ?>" />
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('logo'); ?>
<p>
    <a href="<?php echo e(route('home')); ?>">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</p>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- content -->
    <div class="container p-md-2 mt-md-2">
        <div class="row <?php echo e(Request::has('page') && Request::get('page') > 1 ? 'pt-md-3' : ''); ?>">
            <div class="w-70 page-content">
            <?php if(Request::has('page') && Request::get('page') > 1): ?>
            <div class="">
                            <div class=" row page-text page-category">
                                      <div class=" w-12 social-left">
                          <ul class="social-left-child">
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                <div class="w-78">
                    <?php if(count($datas)>0): ?>
                    <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row pb-2 m-md-0 page-category-child" style="<?php echo e($k!=0 ? 'padding: 1rem 0rem;
                border-top: dashed 1px #e1e1e1;margin-left:0px;    padding-left: 10px;
    padding-right: 10px;' : 'padding-top: 70px;    padding-left: 10px;
    padding-right: 10px;'); ?>">
                                    <div class="col-md-3 col-5">
                                        <div class="">
                                        <a
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>">
                                        <img src="<?php echo e($data->image); ?>" class="img-fluid">
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-7 mobile-pl-0">
                                        <div class="">
                                                                               <h2><a class="title-news "
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>"><?php echo e($data->title); ?></a>
                                        </h2>
                                                                            <p class="mb-0 mobile-hidden"><?php echo html_cut($data->description, 281); ?></p>
                                        </div>
                                    </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p>Không tìm thấy bài viết</p>
                        </div>
                    </div>
                <?php endif; ?>
                                <?php echo e($datas->render('fe.link')); ?>

                </div>
            </div>
            <?php else: ?>
            <h1><?php echo e($datapc->title); ?></h1>
            <div class="col-md-12 cates-top-5">
                <div class="row cates-top-5-row">
                    <div class="col-md-6 cates-top-5-left pr-md-0">
                            <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($k == 0): ?>
                                    <a
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>">
                                        <img src="<?php echo e($data->image); ?>" class="img-fluid">
                                    </a>
                                    <h2><a class="title-news "
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>"><?php echo e($data->title); ?></a>
                                    </h2>
                                    <p class="mb-0"><?php echo html_cut($data->description, 200); ?></p>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="col-md-6 cates-top-5-right">
                        <div class="row">
                                <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($k> 0 & $k<5): ?>
                                    <div class="col-md-6 col-12">
                                    <div class="row">
                                    <div class="col-md-12 col-4">
                                        <div class="">
                                        <a
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>">
                                        <img src="<?php echo e($data->image); ?>" class="img-fluid">
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-8 mobile-pl-0">
                                        <div class="">
                                                                               <h2><a class="title-news "
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>"><?php echo e($data->title); ?></a>
                                        </h2> 
                                        </div>
                                    </div>

                                    </div>
                        </div>
                                    <?php endif; ?>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            <div class=" row page-text page-category pt-md-2">
                                      <div class="w-12 social-left">
                          <ul class="social-left-child">
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                <div class="w-78 pt-0">
                    <?php if(count($datas)>0): ?>
                    <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($k>4): ?>
                    <div class="row m-md-0 pb-2 page-category-child" style="<?php echo e($k!=5 ? 'padding: 1rem 0rem;border-top: dashed 1px #e1e1e1;' : 'padding-top:10px'); ?>">
                                    <div class="col-md-3 col-5">
                                        <div class="">
                                        <a
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>">
                                        <img src="<?php echo e($data->image); ?>" class="img-fluid">
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-7 mobile-pl-0">
                                        <div class="">
                                                                               <h2><a class="title-news "
                                        href="<?php echo e(route('allslug', $data->slugs->slug)); ?>"><?php echo e($data->title); ?></a>
                                        </h2>
                                                                            <p class="mb-0 mobile-hidden"><?php echo html_cut($data->description, 200); ?></p>
                                        </div>
                                    </div>
                            </div>
                            <div class="clear"></div>
                    <?php endif; ?>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p>Không tìm thấy bài viết</p>
                        </div>
                    </div>
                <?php endif; ?>
                <?php echo e($datas->render('fe.link')); ?>

                <!--<?php echo e($datas->onEachSide(5)->links()); ?>-->
                </div>
            </div>
            <?php endif; ?>
            </div>
            </div>
            <div class="w-30">
                <?php echo $__env->make('fe.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>
<!-- end content -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
    nonce="2wLpfQ0N">
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.fe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/fe/catepost.blade.php ENDPATH**/ ?>