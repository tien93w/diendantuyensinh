<?php ?>



<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit User</div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>


                    <form class="form-horizontal" role="form" method="POST"
                        action="<?php echo e(route('user.update',$user->id)); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php echo e(method_field('PATCH')); ?>


                        <div
                            class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="display_name" type="text" class="form-control" name="name"
                                    value="<?php echo e($user->name); ?>" required autofocus>

                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div
                            class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email"
                                    value="<?php echo e($user->email); ?>" required autofocus>

                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>


                </div>
                <div class="col-md-12">
                    <label for="image" class="control-label">Ảnh đại diện</label><br>
                    <span class="form-group-btn">
                        <a id="lfm" data-input="image" data-preview="holderuser" class="btn btn-primary text-white">
                            <i class="fa fa-picture-o"></i> Chọn
                        </a>
                    </span>
                    <input id="image" class="form-control col-md-12" type="text" name="image"
                        value="<?php echo e($user->image); ?>">
                    <div id="holderuser" style="margin-top:15px;max-height:100px;">
                        <img src="<?php echo e(asset($user->image)); ?>" alt="" width="100px">
                    </div>
                </div>

                <div
                    class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" autofocus>
                        <?php if($errors->has('password')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('password')); ?></strong>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div
                    class="form-group<?php echo e($errors->has('confirm-password') ? ' has-error' : ''); ?>">
                    <label for="confirm-password" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="confirm-password" type="password" class="form-control" name="confirm-password"
                            autofocus>
                        <?php if($errors->has('confirm-password')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('confirm-password')); ?></strong>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>


                <div
                    class="form-group<?php echo e($errors->has('roles') ? ' has-error' : ''); ?>">
                    <label for="roles" class="col-md-4 control-label">Roles</label>

                    <div class="col-md-6">

                        <select id="role" name="roles[]" multiple>
                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($role->id); ?>"
                                    <?php echo e(in_array($role->id, $userRole) ? "selected" : null); ?>>
                                    <?php echo e($role->name); ?>

                                </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>

                        <?php if($errors->has('roles')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('roles')); ?></strong>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>

                        <a class="btn btn-link" href="<?php echo e(route('user.index')); ?>">
                            Cancel
                        </a>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>


<script src="<?php echo e(asset('/vendor/laravel-filemanager/js/stand-alone-button.js')); ?>"></script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
    $('#lfm').filemanager('image');
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/user/edit.blade.php ENDPATH**/ ?>