@section('meta')
@if($datapc && $meta)
    <title>{{ $datapc->title ? $datapc->title : "" }}</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description"
        content="{{ $datapc->description ? $datapc->description : $meta->discription }}" />
    <meta property="og:image"
        content="{{ $datapc->image != "" ? $datapc->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png' }}" />
    <meta name="keywords"
        content="{{ $datapc->keywords ? $datapc->keywords : $meta->keysword }}" />
@endif
@endsection
@section('logo')
<p>
    <a href="{{ route('home')}}">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</p>
@endsection
@extends('layouts.fe')
@section('content')
    <!-- content -->
    <div class="container p-md-2 mt-md-2">
        <div class="row {{Request::has('page') && Request::get('page') > 1 ? 'pt-md-3' : ''}}">
            <div class="w-70 page-content">
            <h1>{{ $datapc->title }}</h1>
            <div class=" row page-text page-category pt-md-2">
                    <div class="d-none d-sm-block w-12 social-left">
                          <ul class="social-left-child">
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                    </div>
                <div class="w-78 pt-0">
                        <div class="col-md-12">
                            {!! $datapc->custom->description !!}
                        </div>
                    @if(count($datas)>0)
                    
                        <div class="col-md-12">
                            <div class="row cate-lien-thong">
                                @foreach($datas as $k=>$item)
                                <div class="w-20">
                                <a href="{{ route('allslug',$item->slugs->slug)}}">
                                        <img src="{{ $item->image}}" class="img-fluid" alt="{{ $item->title}}">
                                    </a>
                                    <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                                    <h3>{{ $item->title}}</h3>
                                    </a>
                                </div>
                            <div class="clear"></div>
                            @endforeach
                        </div>


                </div>
                <div class="col-md-12">
                    <div class="page-lienthong-content">
                        {!! $datapc->custom->content !!}
                    </div>
                </div>
                @else
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p>Không tìm thấy bài viết</p>
                        </div>
                    </div>
                @endif
                </div>
            </div>
            </div>
            <div class="w-30">
                @include('fe.sidebar')
            </div>
        </div>
    </div>
<!-- end content -->

@endsection
@section('js')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
    nonce="2wLpfQ0N">
</script>
@endsection
