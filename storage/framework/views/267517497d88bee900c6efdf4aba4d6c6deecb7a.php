<?php $__env->startSection('meta'); ?>
<?php if( $meta): ?>
    <title>
        <?php echo e($meta->title ? $meta->title : "CAO ĐẲNG NẤU ĂN HÀ  NỘI"); ?>

        - 404</title>
    <meta name="description" content="<?php echo e($meta->discription); ?>" />
    <meta name="keywords" content=" <?php echo e($meta->keysword); ?>" />
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('logo'); ?>
<p>
    <a href="<?php echo e(route('home')); ?>">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</p>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="wrap">
        <div id="content-category-wrap " class="top-p20">
            <div id="content" class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <h2 class="categoty-title"> LỖI 404</h2>
                    </div>
                    <div class="col-md-12 top-p20 bot-40">
                        <div class="row">
                            <h1>Lỗi trang không tồn tại</h1>
                            <a href="<?php echo e(route('home')); ?>">Quay lại trang chủ</a>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.fe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/fe/404.blade.php ENDPATH**/ ?>