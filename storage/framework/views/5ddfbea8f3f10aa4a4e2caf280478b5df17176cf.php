<div class="page-siderbar  boder-solid-topsirderbar plr-10">
   <h2 class="page-siderbar-title">THÔNG TIN TUYỂN SINH</h2>

   <div class="row m-0 ">
      <ul class="pl-0">
         <?php //dd($tuyensinh);?>
         <?php $__currentLoopData = $tuyensinh; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="siderbar-item-child">
               <div class="col-md-4 p-0">
                  <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                     <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                  </a>
               </div>
               <div class="col-md-8 pr-0">
                  <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                     <h2><?php echo e($item->title); ?></h2>
                  </a>
               </div>
            </li>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </ul>
   </div>
</div>


<div class="page-siderbar-2 pt-3 mt-3 mb-3 boder-solid-topsirderbar plr-10">
   <h2 class="page-siderbar-title">CÁC TRƯỜNG LIÊN THÔNG</h2>

   <div class="row m-0 ">
      <ul class="pl-0" style="    display: contents;">
         <?php $__currentLoopData = $lienthong; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <li class="pt-1 col-4 p-0 siderbar-item-child-2">
               <div class="col-md-12 p-0">
                  <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                  <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                  </a>
               </div>
               <div class="col-md-12 p-0">
                  <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                  <h2><?php echo substr($item->title,
                                        0, 58); ?></h2>
               </a>
               </div>

         </li>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      </ul>
   </div>
</div>

<div class="page-ads-img  sticky-top-ads">
   <div class="">
      <div class="row m-0">
<!-- diendantuyensinh24h.com - 300x600 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-1895892504965300"
     data-ad-slot="3410095460"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
      </div>
   </div>

</div>
<script>

</script><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/fe/sidebar.blade.php ENDPATH**/ ?>