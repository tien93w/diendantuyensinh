<?php $__env->startSection('meta'); ?>
<?php if($datapc && $meta): ?>
    <title><?php echo e($datapc->name ? $datapc->name : ""); ?></title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description" content="<?php echo e($meta->discription); ?>" />
    <meta name="keywords" content="<?php echo e($meta->keysword); ?>" />
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('home'); ?>
<p id="title">
    <a href="<?php echo e(route('home')); ?>"></a>
</p>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="wrap">
        <div id="content-category-wrap " class="top-p20">
            <div id="content" class="row">
                <div class="col-md-9">
                    <div class="col-md-12">
                        <h2 class="categoty-title"><?php echo e($datapc->name); ?></h2>
                    </div>
                    <div class="col-md-12 top-p20">
                        <div class="row">
                            <?php if(count($datas)>0): ?>
                                <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="widget-tuyendung">
                                        <div class="col-md-4">
                                            <a
                                                href="<?php echo e(route('allslug', $data->slugs->slug)); ?>">
                                                <img src="<?php echo e($data->image); ?>">
                                            </a>
                                        </div>
                                        <div class="col-md-8">
                                            <h2><a class="title-news "
                                                    href="<?php echo e(route('allslug', $data->slugs->slug)); ?>"><?php echo e($data->title); ?></a>
                                            </h2>
                                            <span class="time"></span>
                                            <p><?php echo preg_replace('/\s+?(\S+)?$/', '', substr($data->description, 0,
                                                401)); ?></p>
                                            <p></p>
                                            <span class="xt">
                                                <a
                                                    href="<?php echo e(route('allslug', $data->slugs->slug)); ?>">Xem
                                                    tiếp</a>
                                            </span>
                                        </div>

                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php else: ?>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <p>Không tìm thấy bài viết</p>
                                    </div>

                                </div>

                            <?php endif; ?>

                            <?php echo e($datas->links()); ?>

                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <?php echo $__env->make('fe.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div id="positionneo"></div>
            </div>

        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.fe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/fe/tagpost.blade.php ENDPATH**/ ?>