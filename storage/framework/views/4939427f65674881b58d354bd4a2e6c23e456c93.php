

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Sửa bài viết
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item"><a
                            href="<?php echo e(route('allslug', $data2->slugs->slug)); ?>"
                            target="_blank">XEM TRƯỚC</a>
                    </li>
                </ul>
            </div>

        </div>
        <div class="m-portlet__body">

            <?php echo $__env->make('admin.errors', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


            <form class="form-horizontal" style="width:100%" role="form" method="POST"
                action="<?php echo e(route('post.update',$data2->id)); ?>">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('PATCH')); ?>

                <div class="row">
                    <div class="col-md-9">
                        <div
                            class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                            <label for="title" class="col-md-12 control-label">TIÊU ĐỀ BÀI VIẾT</label>

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title"
                                    value="<?php echo e($data2->title); ?>" required autofocus>

                                <?php if($errors->has('title')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div
                            class="form-group<?php echo e($errors->has('slug') ? ' has-error' : ''); ?>">
                            <label for="slug" class="col-md-12 control-label">Slug BÀI VIẾT</label>

                            <div class="col-md-12">
                                <input id="slug" type="text" class="form-control" name="slug"
                                    value="<?php echo e($data2->slugs->slug); ?>">

                                <?php if($errors->has('slug')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('slug')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-12 video-postcustom"
                        style="display: <?php echo e($data2->slugs->type == 'new' ? 'none':'block'); ?>">
                            <div class="form-group">
                                <label for="video">Video ĐẠI DIỆN</label>
                            <textarea name="video" class="form-control" id="video" rows="4"><?php echo e($data2->video); ?></textarea>
                            </div>
                        </div>
                        <div
                            class="form-group<?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-12 control-label">MÔ TẢ NGẮN</label>

                            <div class="col-md-12">
                                <textarea id="description" rows="7" name="description"
                                    class="form-control my-editor"><?php echo e($data2->description); ?></textarea>
                                <?php if($errors->has('description')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('description')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div
                            class="form-group<?php echo e($errors->has('content') ? ' has-error' : ''); ?>">
                            <label for="content" class="col-md-12 control-label">MÔ TẢ </label>

                            <div class="col-md-12">
                                <textarea id="content" rows="40" name="content"
                                    class="form-control  my-editor"><?php echo e(old('content') ? old('content') : $data2->content); ?></textarea>
                                <?php if($errors->has('content')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('contents')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="col-md-12 link-postcustom"
                            style="display: <?php echo e($data2->slugs->type == 'new' ? 'none':'block'); ?>">
                            <h2>Linh liên kết</h2>
                            <div class="form-group">
                                <label for="">Link liên kết 1</label>
                                <input type="text" class="form-control" name="link1" placeholder="Nhap link">
                            </div>
                            <div class="form-group">
                                <label for="">Link liên kết 2</label>
                                <input type="text" class="form-control" name="link2" placeholder="Nhap link">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2>SEO
                                <span style="font-size: 40px; float: right" class="" data-toggle="collapse"
                                    data-target="#demo"><i class="fa fa-angle-double-down"
                                        style="font-size: 40px;color: #0b51c5" aria-hidden="true"></i></span>
                            </h2>
                        </div>
                        <div id="demo" class="collapse">
                            <div
                                class="form-group<?php echo e($errors->has('meta_title') ? ' has-error' : ''); ?>">
                                <label for="meta_title" class="col-md-12 control-label">Meta Title</label>

                                <div class="col-md-12">
                                    <div id="ameta_title"></div>
                                    <input rows="7" id="meta_title" name="meta_title" class="form-control"
                                        value="<?php echo e($data2->meta_title); ?>"/>
                                    <?php if($errors->has('meta_title')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('meta_title')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div
                                class="form-group<?php echo e($errors->has('keywords') ? ' has-error' : ''); ?>">
                                <label for="keywords" class="col-md-12 control-label">Meta Keywords</label>

                                <div class="col-md-12">
                                    <div id="akeywords"></div>
                                    <input rows="7" id="keywords" name="keywords" class="form-control"
                                        value="<?php echo e($data2->keywords); ?>"/>
                                    <?php if($errors->has('keywords')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('keywords')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div
                                class="form-group<?php echo e($errors->has('mdescription') ? ' has-error' : ''); ?>">
                                <label for="mdescription" class="col-md-12 control-label">Meta Description</label>

                                <div class="col-md-12">
                                    <div id="amdescription"></div>
                                    <textarea rows="4" id="mdescription" name="mdescription"
                                        class="form-control"><?php echo e($data2->mdescription); ?></textarea>
                                    <?php if($errors->has('mdescription')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('mdescription')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group row" style="margin:0px">

                             <div class="col-md-12">
                                            <div class="m-form__group form-group row">
                                                <label class="col-12 col-form-label">Bài viết nổi bật</label>
                                                <div class="col-12">
                                                    <span class="m-switch m-switch--success">
                                                        <label>
                                                         <input type="checkbox"  <?php echo e($data2->feature != 0 ? 'checked' : null); ?>  value="1" name="feature">

                                                        <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                                <label class="col-12 col-form-label">Public bài viết</label>
                                                <div class="col-12">
                                                    <span class="m-switch m-switch--warning">
                                                        <label>
                                                         <input type="checkbox"  <?php echo e($data2->public != 0 ? 'checked' : null); ?>  value="1" name="public">
                                                        <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                            <div class="col-12">
                                <h2>Thời gian public</h2>
                                <div class="form-group">
                                    <input type="datetime-local" class="form-control" name="published_at" value="<?php echo e(old('published_at') ? old('published_at') : date('Y-m-d\Th:m:s',  strtotime($data2->published_at))); ?>" placeholder="">
                                </div>
                            </div>

                                        <div class="col-md-12 pb-3">
                                            <h3>Post template</h3>
                                            <select name="type" id="" onchange="pageType(this)" class="form-control">
                                                <option
                                                    <?php echo e($data2->slugs->type == 'new' ? 'selected' :''); ?>

                                                    value="new">Default</option>
                                                <option
                                                    <?php echo e($data2->slugs->type == 'landing' ? 'selected' :''); ?>

                                                    value="landing">Landing</option>
                                                <option
                                                    <?php echo e($data2->slugs->type == 'newcustom' ? 'selected' :''); ?>

                                                    value="newcustom">Custom Post</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <h3>No Index Google</h3>
                                        <input type="checkbox" <?php echo e($data2->index_seo != 1 ? null : 'checked'); ?> value="1" name="index_seo"> Index Google
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo Form::label('tag', 'Tags'.'', ['class' => 'control-label']); ?>

                
                                            <?php echo Form::select('tag[]', $tags, old('tag') ? old('tag') :
                                            $data2->tag->pluck('id')->toArray(), ['class' => 'form-control select2', 'multiple' =>
                                            'multiple', 'id' => 'selectall-tag' ]); ?>

                                            <p class="help-block"></p>
                                            <?php if($errors->has('tag')): ?>
                                                <p class="help-block">
                                                    <?php echo e($errors->first('tag')); ?>

                                                </p>
                                            <?php endif; ?>
                                            <div style="color: #0E0EFF;padding-bottom: 30px">
                                                <a style="background-color: #fdf7f7" type="button" class="btn  btn-xs"
                                                    id="selectbtn-tag">
                                                    Chọn tất cả tag
                                                </a>
                                                <a style="background-color: #fdf7f7" type="button" class="btn btn-xs"
                                                    id="deselectbtn-tag">
                                                    Xoá tất cả tag
                                                </a>
                                            </div>
                
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo Form::label('cate', 'Category'.'', ['class' => 'control-label']); ?>

                                            <select class="form-control select2" name="cate[]" multiple="multiple">
                                                <?php categoryParentSelect2($data ,$parent = 0, $str="",$data2->cates->pluck('id')->toArray()); ?>
                                            </select>
                                            <p class="help-block"></p>
                                            <?php if($errors->has('cate')): ?>
                                                <p class="help-block">
                                                    <?php echo e($errors->first('cate')); ?>

                                                </p>
                                            <?php endif; ?>
                                        </div>
                            <div class="col-md-12">
                                <h2><label for="image" class="control-label">ẢNH ĐẠI DIỆN</label><br></h2>
                                
                                <span class="form-group-btn">
                                    <a id="lfm" data-input="image" data-preview="holder"
                                        class="btn btn-primary text-white">
                                        <i class="fa fa-picture-o"></i> Chọn
                                    </a>
                                </span>
                                <input id="image" class="form-control col-md-12" type="text" name="image"
                                    value="<?php echo e($data2->image); ?>">
                                <div id="holder" style="margin-top:15px;max-height:100px;">
                                    <img src="<?php echo e(asset($data2->image)); ?>" alt="">
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="padding-top: 33px">
                        <button type="submit" class="btn btn-primary">
                            SỬA BÀI VIẾT
                        </button>

                        <a class="btn btn-link" href="<?php echo e(route('post.index')); ?>">
                            HUỶ
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="<?php echo e(asset('/vendor/laravel-filemanager/js/stand-alone-button.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.4.1/tinymce.min.js" integrity="sha512-c46AnRoKXNp7Sux2K56XDjljfI5Om/v1DvPt7iRaOEPU5X+KZt8cxzN3fFzemYC6WCZRhmpSlZvPA1pttfO9DQ==" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('/admin/js/tiny.js')); ?>"></script>
<script type="text/javascript">
    function pageType(obj) {
        var value = obj.value;
        if (value === 'newcustom') {
            $('.link-postcustom').css({
                'display': 'block'
            })
        }
        if (value === 'new') {
            $('.link-postcustom').css({
                'display': 'none'
            })
        }
        if (value === 'newcustom') {
            $('.video-postcustom').css({
                'display': 'block'
            })
        }
        if (value === 'new') {
            $('.video-postcustom').css({
                'display': 'none'
            })
        }
    }
    $('#lfm').filemanager('image');
</script>
<script>
    $("#selectbtn-tag").click(function () {
        $("#selectall-tag > option").prop("selected", "selected");
        $("#selectall-tag").trigger("change");
    });
    $("#deselectbtn-tag").click(function () {
        $("#selectall-tag > option").prop("selected", "");
        $("#selectall-tag").trigger("change");
    });
    $("#selectbtn-cate").click(function () {
        $("#selectall-cate > option").prop("selected", "selected");
        $("#selectall-cate").trigger("change");
    });
    $("#deselectbtn-cate").click(function () {
        $("#selectall-cate > option").prop("selected", "");
        $("#selectall-cate").trigger("change");
    });

    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
<script>
    $('#meta_title').keyup(function () {
        $('#ameta_title').text('bạn đã nhập ' + this.value.length + 'ký tự');
    });
    $('#keywords').keyup(function () {
        $('#akeywords').text('bạn đã nhập ' + this.value.length + ' ký tự');
    });
    $('#mdescription').keyup(function () {
        $('#amdescription').text('bạn đã nhập ' + this.value.length + ' ký tự');
    });
</script>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/post/edit.blade.php ENDPATH**/ ?>