<?php echo $__env->make('layouts.fe.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('meta'); ?>
</head>
<body>
<?php echo $__env->make('include.afterbody', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="container">
        <div class="row topbar">
            <div class="col-xs-5 col-md-5 topbar-left">
                <ul>
                    <li><span>Connect with us:</span></li>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-xs-7 col-md-7 topbar-right">
                <ul>
                    <li>
                        <a href=""><i class="fa fa-handshake-o" aria-hidden=""></i>Hợp tác tuyển sinh</a>
                        <a href=""><i class="fa fa-bullhorn" aria-hidden="true"></i>Liên hệ quảng cáo</a>
                        <a href=""><i class="fa fa-lock" aria-hidden="true"></i>Chính sách bảo mật</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
        <nav class="navbar navbar-custom navbar-static-top navbar-fixed-top" role="navigation">
        <div class="navbar-header">
           <div class="navbar-right">
              <ul class="navbar-toggle icon-nav">
                 <li>
                    <a class="ttgd" title="tin tức giáo dục" href="https://diendantuyensinh24h.com/tin-tuc/">
                       <img title="tin tức giáo dục" alt="tin tức giáo dục" class="icon-news lazyloaded" src="<?php echo e(asset('fe/images/ico1.png')); ?>" data-lazy-src="<?php echo e(asset('fe/images/ico1.png')); ?>" data-was-processed="true">
                       <noscript><img title="tin tức giáo dục" alt="tin tức giáo dục" class="icon-news" src="<?php echo e(asset('fe/images/ico1.png')); ?>" alt="" /></noscript>
                    </a>
                 </li>
                 <li>
                    <a href="#menu" data-toggle="collapse" class="" aria-expanded="true">
                    <img class="icon-toggle lazyloaded" src="<?php echo e(asset('images/ico2.png')); ?>" alt="danh mục" title="danh-mục" data-lazy-src="<?php echo e(asset('images/ico2.png')); ?>" data-was-processed="true">
                    <noscript><img class="icon-toggle" src="<?php echo e(asset('images/ico2.png')); ?>" alt="danh mục" title="danh-mục" /></noscript>
                    </a>
                 </li>
              </ul>
           </div>
           <a class="navbar-brand" href="https://diendantuyensinh24h.com">
           <img class="img-fluid logo lazyloaded" src="<?php echo e(asset('images/logo-mobi.png')); ?>" alt="logo" title="logo" data-lazy-src="<?php echo e(asset('images/logo-mobi.png')); ?>" data-was-processed="true">
              <noscript><img class="logo" src="<?php echo e(asset('images/logo-mobi.png')); ?>" alt="logo" title="logo" /></noscript>
           </a>
        </div>
        <div class="clear"></div>
        <div class="view">
           <div class="navbar-collapse collapse in" id="menu" aria-expanded="true" style="">
              <ul class="menu_cat_main">
                 <li> <a data-toggle="modal"  data-target="#colldh" href="https://diendantuyensinh24h.com/dai-hoc/"> <span><i class="fa fa-graduation-cap 2x" aria-hidden="true"></i></span> <span class="txt">Đại học</span> </a></li>
                 <li> <a data-toggle="modal"  data-target="#collcd" href="https://diendantuyensinh24h.com/cao-dang/"> <span><i class="fa fa-graduation-cap 2x" aria-hidden="true"></i></span> <span class="txt">Cao đẳng</span> </a></li>
                 <li> <a data-toggle="modal"  data-target="#colltc" href="https://diendantuyensinh24h.com/trung-cap/"> <span><i class="fa fa-graduation-cap 2x" aria-hidden="true"></i></span> <span class="txt">Trung cấp</span> </a></li>
                 <li> <a href="https://diendantuyensinh24h.com/lien-thong-dai-hoc"> <span><i class="fa fa-graduation-cap 2x" aria-hidden="true"></i></span> <span class="txt">Liên thông</span> </a></li>
                 <li> <a  data-toggle="modal"  data-target="#colldc"  href="https://diendantuyensinh24h.com/diem-chuan"> <span><i class="fa fa-graduation-cap 2x" aria-hidden="true"></i></span> <span class="txt">Điểm chuẩn</span> </a></li>
                 <li> <a href="https://diendantuyensinh24h.com/tin-tuc-dai-hoc"> <span><i class="fa fa-graduation-cap 2x" aria-hidden="true"></i></span> <span class="txt">Tin giáo dục</span> </a></li>
              </ul>
                            <div class="modal fade" id="colldc" role="dialog"  tabindex="-1" >
                 <div class="modal-dialog"  role="document">
                    <div class="modal-content">
                       <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">×</button>
                          <h4 class="modal-title">Danh sách các trường Đại học theo khu vực</h4>
                       </div>
                       <div class="modal-body">
                          <ul class="ovh menu_cat_main">
                             <li> <a href="https://diendantuyensinh24h.com/diem-chuan-cac-truong-dai-hoc-khu-vuc-ha-noi"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Khu vực Hà Nội</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/diem-chuan-dai-hoc-khu-vuc-tphcm"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Khu vực TP.HCM</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/diem-chuan-cac-truong-dai-hoc-tai-mien-bac"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Khu vực Miền Bắc</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/diem-chuan-dai-hoc-khu-vuc-mien-trung-tay-nguyen"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Khu vực Miền Trung</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/diem-chuan-dai-hoc-khu-vuc-mien-nam"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Khu vực Miền Nam</span> </a></li>
                                <li> <a href="https://diendantuyensinh24h.com/du-kien-diem-chuan"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Khu vực Miền Nam</span> </a></li>
                          </ul>
                       </div>
                       <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button></div>
                    </div>
                 </div>
              </div>
              <div class="modal fade" id="colldh" role="dialog"  tabindex="-1" >
                 <div class="modal-dialog"  role="document">
                    <div class="modal-content">
                       <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">×</button>
                          <h4 class="modal-title">Danh sách các trường Đại học theo khu vực</h4>
                       </div>
                       <div class="modal-body">
                          <ul class="ovh menu_cat_main">
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-tp-ha-noi"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Đại học khu vực Hà Nội</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-tp-hcm"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Đại học khu vực TP.HCM</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-bac"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Đại học khu vực Miền Bắc</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-trung"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Đại học khu vực Miền Trung</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-nam"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Đại học khu vực Miền Nam</span> </a></li>
                          </ul>
                       </div>
                       <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button></div>
                    </div>
                 </div>
              </div>
              <div class="modal fade" id="collcd"  tabindex="-1"  role="dialog">
                 <div class="modal-dialog">
                    <div class="modal-content">
                       <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">×</button>
                          <h4 class="modal-title">Danh sách các trường Cao Đẳng theo khu vực</h4>
                       </div>
                       <div class="modal-body">
                          <ul class="ovh menu_cat_main">
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-tp-ha-noi-cao-dang"> <span><i class="fa fa-university" aria-hidden="false"></i></span> <span class="txt">Cao đẳng khu vực Hà Nội</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-tp-hcm-cao-dang"> <span><i class="fa fa-university" aria-hidden="false"></i></span> <span class="txt">Cao đẳng khu vực TP.HCM</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-bac-cao-dang"> <span><i class="fa fa-university" aria-hidden="false"></i></span> <span class="txt">Cao đẳng khu vực Miền Bắc</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-trung-cao-dang"> <span><i class="fa fa-university" aria-hidden="false"></i></span> <span class="txt">Cao đẳng khu vực Miền Trung</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-nam-cao-dang"> <span><i class="fa fa-university" aria-hidden="false"></i></span> <span class="txt">Cao đẳng khu vực Miền Nam</span> </a></li>
                          </ul>
                       </div>
                       <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button></div>
                    </div>
                 </div>
              </div>
              <div class="modal fade" id="colltc" role="dialog">
                 <div class="modal-dialog">
                    <div class="modal-content">
                       <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">×</button>
                          <h4 class="modal-title">Danh sách các trường Trung Cấp theo khu vực</h4>
                       </div>
                       <div class="modal-body">
                          <ul class="ovh menu_cat_main">
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-tp-ha-noi-trung-cap"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Trung Cấp khu vực Hà Nội</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-tp-hcm-trung-cap"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Trung Cấp khu vực TP.HCM</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-bac-trung-cap"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Trung Cấp khu vực Miền Bắc</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-trung-trung-cap"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Trung Cấp khu vực Miền Trung</span> </a></li>
                             <li> <a href="https://diendantuyensinh24h.com/khu-vuc-mien-nam-trung-cap"> <span><i class="fa fa-university" aria-hidden="true"></i></span> <span class="txt">Trung Cấp khu vực Miền Nam</span> </a></li>
                          </ul>
                       </div>
                       <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button></div>
                    </div>
                 </div>
              </div>
              <div class="menu_cat_sub">
                 <h4>Các Chuyên Mục Khác</h4>
                 <ul>
                    <li> <a href="https://diendantuyensinh24h.com/tin-tuc-thpt">THPT</a></li>
                    <li> <a href="https://diendantuyensinh24h.com/khoi-thi">Khối thi</a></li>
                    <li> <a href="https://diendantuyensinh24h.com/tieu-diem-mua-thi/">Mùa thi</a></li>
                    <li> <a href="https://diendantuyensinh24h.com/chung-chi">Chứng chỉ</a></li>
                    <li> <a href="https://diendantuyensinh24h.com/tin-tuc-lien-thong">Tin tức liên thông</a></li>
                    <li> <a href="https://diendantuyensinh24h.com/tin-tuc-dai-hoc">Tin tức đại học</a></li>            
                    <li> <a href="https://diendantuyensinh24h.com/lao-dong-du-hoc">Lao động - Du học</a></li>
                    <li> <a href="https://diendantuyensinh24h.com/du-kien-diem-chuan">Dự kiến điểm chuẩn</a></li>
                    <li> <a href="https://diendantuyensinh24h.com/lien-he-quang-cao/">Liên hệ quảng cáo</a></li>
                 </ul>
              </div>
           </div>
        </div>
     </nav>
    <div class="container-fluid logo-banner">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-md-4  col-sm-12">
                    <?php echo $__env->yieldContent('logo'); ?>
                </div>
                <div class="col-md-8">
                    <div class="banner-adsaaaa">
<!-- diendantuyensinh24h.com - 728x90 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-1895892504965300"
     data-ad-slot="3412736565"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid main-menu p-xs-0">
        <div class="container p-xs-0">
            <div class="">

                <nav class="navbar navbar-expand-md  navbar-light pl-lg-0">
                <a class="navbar-brand  navbar-home" href="<?php echo e(route('home')); ?>"><i class="fa fa-home" aria-hidden="true"></i></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse 	.d-block .d-sm-none" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <?php showMenus($menus,0,url('/'),$char='nav-item','nav-item','nav-item')?>
                        </ul>
                    </div>
                </nav>

            </div>
        </div>
    </div>
    <!-- content -->
    <div class="wrap">
        <?php echo $__env->yieldContent('content'); ?>
    </div>
    <!-- end content -->
<?php echo $__env->make('layouts.fe.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('include.beforebodyend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
    nonce="SSfJPm6g"></script>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="<?php echo e(asset('js/main.js')); ?>"></script>
<?php echo $__env->yieldContent('js'); ?>

</html><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/layouts/fe.blade.php ENDPATH**/ ?>