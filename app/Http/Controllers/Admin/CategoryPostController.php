<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Slug\SlugRepositoryInterface;
use DataTables;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class CategoryPostController extends Controller
{
    protected $cateRepository;
    protected $repoSlug;
    public function __construct(CategoryRepositoryInterface $cateRepository,SlugRepositoryInterface $repoSlug)
    {
        $this->cateRepository = $cateRepository;
        $this->repoSlug = $repoSlug;
        $this->middleware('permission:postc-list|postc-create|postc-edit|postc-delete', ['only' => ['index','store']]);
        $this->middleware('permission:postc-create', ['only' => ['create','store']]);
        $this->middleware('permission:postc-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:postc-delete', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->cateRepository->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('parent', function($row){
                    $rolename = "";
                    if(!empty($row->parent)){
                        $rolename = $row->parent['title'];
                    }else
                    {
                        $rolename = "None";
                    }
                    return $rolename;
                })
                ->rawColumns(['parent'])
                ->addColumn('slug', function($row){
                        return $row->slugs->slug;
                })
                ->rawColumns(['slug'])
                ->addColumn('action', function($row){
                    $btn = '<a  href="'.route('postcate.edit', $row->id).'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                    <i class="la la-edit"></i>
                    </a> <a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                    <i class="la la-close"></i>
                    </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.postcate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {   $datas = $this->cateRepository->all();
        return view('admin.postcate.create',compact('datas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:slugs',
        ]);
        $input = $request->all();
        $idSlug = $this->repoSlug->insertGetId($request->only(['slug','type']));
        $input['slug_id'] = $idSlug;
        $data = $this->cateRepository->create($input);
        if($data->wasRecentlyCreated === false){
            $this->repoSlug->delete($idSlug);
        }else{
            return redirect()->route('postcate.index')
            ->with('success','Tọa mới danh mục thành công');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data = $this->cateRepository->find($id);
        return view('admin.postcate.show',compact('data'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $datas = $this->cateRepository->all();
        $data = $this->cateRepository->find($id);
        return view('admin.postcate.edit',compact('data','datas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->cateRepository->find($id);
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:slugs,slug,'.$data->slug_id ,
        ]);
        $this->cateRepository->update($id,$request->except('slug','_token','_method','content','cdescription'));
        $this->repoSlug->update($data->slug_id,['slug'=>$request->slug]);
        $data->custom()->update(['description'=>$request->cdescription,'content'=>$request->content]);
        return back()
            ->with('success','Sửa danh mục thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->cateRepository->deleteWithSlug($id);
        return redirect()->route('postcate.index')
            ->with('success','Xóa danh mục thành công');
    }
}
