<?php ?>


<?php $__env->startSection('content'); ?>
<div class="container-fluid">
      <div class="col-md-12">
            <?php if($message = Session::get('success')): ?>
                  <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        <strong><?php echo e($message); ?></strong>
                  </div>
            <?php endif; ?>
      </div>
      <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                              <h3 class="m-portlet__head-text">
                                    Link trong bài viết
                              </h3>
                        </div>
                  </div>
                  <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                              <li class="m-portlet__nav-item">
                              </li>
                        </ul>
                  </div>

            </div>
            <div class="m-portlet__body">

                  <!--begin: Search Form -->
                  <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                              <div class="col-xl-8 order-2 order-xl-1">

                              </div>
                              <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="<?php echo e(route('link.create')); ?>"
                                          class="btn btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                          <span>
                                                <i class="la la-cart-plus"></i>
                                                <span>Thêm link bài viết</span>
                                          </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                              </div>
                        </div>
                  </div>
                  <!--end: Search Form -->

                  <!--begin: Datatable -->
                  <div class="m_datatable" id="child_data_ajax">

                        <table class="data-table m-datatable__table">
                              <thead>
                                    <tr>
                                          <th>No</th>
                                          <th>Link Title</th>
                                          <th width="120px">Action</th>
                                    </tr>
                              </thead>
                              <tbody>
                              </tbody>
                        </table>
                  </div>
                  <!--end: Datatable -->
            </div>
      </div>

      <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>

            <script type="text/javascript">
                  $(document).ready(function () {
                        $.ajaxSetup({
                              headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              }
                        });
                        var table = $('.data-table').DataTable({
                              processing: true,
                              serverSide: true,
                              pageLength:50,
                              ajax: "<?php echo e(route('link.index')); ?>",
                              columns: [{
                                          data: 'DT_RowIndex',
                                          name: 'DT_RowIndex'
                                    },
                                    {
                                          data: 'title',
                                          name: 'title'
                                    },
                                    {
                                          data: 'action',
                                          name: 'action',
                                          orderable: false,
                                          searchable: false
                                    },
                              ]
                        });

                        $('body').on('click', '.deleteUser', function () {
                              //$(".deleteUser").click(function(){

                              var page_id = $(this).data("id");
                              var r = confirm("Bạn chắc chắn muốn xóa !");
                              if (r == true) {
                                    $.ajax({
                                          type: "POST",
                                          data: {
                                                _method: 'delete'
                                          },
                                          url: route('link.destroy', {
                                                id: page_id
                                          }),
                                          success: function (data) {
                                                table.draw();
                                          },
                                          error: function (data) {
                                                console.log('Error:', data);
                                          }
                                    });
                              } else {
                                    return false;
                              }

                        });
                        $(".alert").fadeTo(2000, 500).slideUp(500, function () {
                              $(".alert").slideUp(500);
                        });

                  });
            </script>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/link/index.blade.php ENDPATH**/ ?>