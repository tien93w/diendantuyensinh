<?php $__env->startSection('meta'); ?>
<?php if($datapc && $meta): ?>
    <title><?php echo e($datapc->title ? $datapc->title : ""); ?></title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description"
        content="<?php echo e($datapc->description ? $datapc->description : $meta->discription); ?>" />
    <meta property="og:image"
        content="<?php echo e($datapc->image != "" ? $datapc->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png'); ?>" />
    <meta name="keywords"
        content="<?php echo e($datapc->keywords ? $datapc->keywords : $meta->keysword); ?>" />
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('logo'); ?>
<p>
    <a href="<?php echo e(route('home')); ?>">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</p>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- content -->
    <div class="container p-md-2 mt-md-2">
        <div class="row <?php echo e(Request::has('page') && Request::get('page') > 1 ? 'pt-md-3' : ''); ?>">
            <div class="w-70 page-content">
            <h1><?php echo e($datapc->title); ?></h1>
            <div class=" row page-text page-category pt-md-2">
                    <div class="d-none d-sm-block w-12 social-left">
                          <ul class="social-left-child">
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                    </div>
                <div class="w-78 pt-0">
                        <div class="col-md-12">
                            <?php echo $datapc->custom->description; ?>

                        </div>
                    <?php if(count($datas)>0): ?>
                    
                        <div class="col-md-12">
                            <div class="row cate-lien-thong">
                                <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="w-20">
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                        <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="<?php echo e($item->title); ?>">
                                    </a>
                                    <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                    <h3><?php echo e($item->title); ?></h3>
                                    </a>
                                </div>
                            <div class="clear"></div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>


                </div>
                <div class="col-md-12">
                    <div class="page-lienthong-content">
                        <?php echo $datapc->custom->content; ?>

                    </div>
                </div>
                <?php else: ?>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p>Không tìm thấy bài viết</p>
                        </div>
                    </div>
                <?php endif; ?>
                </div>
            </div>
            </div>
            <div class="w-30">
                <?php echo $__env->make('fe.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>
<!-- end content -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
    nonce="2wLpfQ0N">
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.fe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/fe/pagelienthong.blade.php ENDPATH**/ ?>