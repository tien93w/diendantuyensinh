<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostLink extends Model
{
    //
    protected $fillable = ['title','link','alt'];
}
