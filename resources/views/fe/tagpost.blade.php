@section('meta')
@if($datapc && $meta)
    <title>{{ $datapc->name ? $datapc->name : "" }}</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description" content="{{ $meta->discription }}" />
    <meta name="keywords" content="{{ $meta->keysword }}" />
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
    <a href="{{ route('home') }}"></a>
</p>
@endsection
@section('content')

<div class="container">
    <div class="wrap">
        <div id="content-category-wrap " class="top-p20">
            <div id="content" class="row">
                <div class="col-md-9">
                    <div class="col-md-12">
                        <h2 class="categoty-title">{{ $datapc->name }}</h2>
                    </div>
                    <div class="col-md-12 top-p20">
                        <div class="row">
                            @if(count($datas)>0)
                                @foreach($datas as $data)
                                    <div class="widget-tuyendung">
                                        <div class="col-md-4">
                                            <a
                                                href="{{ route('allslug', $data->slugs->slug) }}">
                                                <img src="{{ $data->image }}">
                                            </a>
                                        </div>
                                        <div class="col-md-8">
                                            <h2><a class="title-news "
                                                    href="{{ route('allslug', $data->slugs->slug) }}">{{ $data->title }}</a>
                                            </h2>
                                            <span class="time"></span>
                                            <p>{!! preg_replace('/\s+?(\S+)?$/', '', substr($data->description, 0,
                                                401))!!}</p>
                                            <p></p>
                                            <span class="xt">
                                                <a
                                                    href="{{ route('allslug', $data->slugs->slug) }}">Xem
                                                    tiếp</a>
                                            </span>
                                        </div>

                                    </div>
                                @endforeach

                            @else
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <p>Không tìm thấy bài viết</p>
                                    </div>

                                </div>

                            @endif

                            {{ $datas->links() }}
                        </div>
                    </div>
                </div>
                {{-- end-md 9 --}}
                <div class="col-md-3">
                    @include('fe.sidebar')
                </div>
                <div id="positionneo"></div>
            </div>

        </div>
    </div>
</div>

@endsection