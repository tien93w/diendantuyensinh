<?php

namespace App\Providers;

use App\Shortcodes\FormShortcode;
use App\Shortcodes\LinkShortcode;
use Illuminate\Support\ServiceProvider;
use Shortcode;

class ShortcodesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Shortcode::register('form', FormShortcode::class);
        Shortcode::register('aform', LinkShortcode::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
