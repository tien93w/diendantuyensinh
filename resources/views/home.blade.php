@extends('layouts.fe')
@section('meta')
@if ($meta)
<title>{{$meta->title ? $meta->title : "CAO ĐẲNG NẤU ĂN HÀ  NỘI"}}</title>
<meta name="robots" content="noindex, nofollow" />
<meta property="og:image"           content="{{ $meta->image != "" ? $meta->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png' }}" />
<meta name="keywords" content="{{ $meta->keywords ? $meta->keywords : $meta->keysword}}"/>
<meta name="description" content="{{ $meta->discription ? $meta->discription : ""}}"/>
@endif
@endsection
@section('logo')
<h1>
    <a href="{{ route('home')}}">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</h1>
@endsection
@section('content')
<!-- block one -->
<div class="container-fluid block-one">
    <div class="container">
        <div class="row">
            <div class="w-68">
                <div class="w-58 block-one-left">
                    @foreach ($postnews as $item)
                        @if ($loop->first)
                        <a href="{{ route('allslug',$item->slugs->slug)}}">
                        <img src="{{$item->image}}" class="img-fluid" alt="">
                        </a>
                        <a href="{{ route('allslug',$item->slugs->slug)}}">
                            <h2>{{$item->title}}</h2>
                        </a>
                        @endif
                    @endforeach
                    <div>
                        <ul>
                            @foreach ($postnews as $k=>$item)
                            @if ($k <3 && $k >0)
                            <li><a href="{{ route('allslug',$item->slugs->slug)}}">{{$item->title}}</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="w-42 block-one-center">
                    <h2>TIN TỨC TUYỂN SINH</h2>
                    <ul>
                        @foreach ($postnews as $k=>$item)
                        @if ($k > 2)
                        <li>
                            <div class="col-md-12"  style="padding-left: 15px;
                            margin-top: -.5rem;">
                                <div class="row">
                            <div class="col-4 d-block d-sm-none">
                                <img class="img-fluid" src="{{ $item->image}}" alt="{{ $item->title}}"/>
                            </div>
                            <div class="col-8 col-md-12"><a href="{{ route('allslug',$item->slugs->slug)}}">{{$item->title}}</a><div>
                                </div>
                                </div>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="w-32 block-one-right">
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
            </div>

        </div>
    </div>
</div>
<!-- end block one -->
<!-- block two -->
<div class="container block-two">

    <div class="row">
        <div class="w-68">
            <div class="mt-15">

                <ul class="nav nav-tab-home" role="tablist">
                    <li role="presentation" class="active"><a href="#diem-chuan" aria-controls="home" role="tab" data-toggle="tab">                <div class="row block-two-title">
                        <h2>Điểm chuẩn Đại học</h2>
                    </div></a></li>
                    <li role="presentation"><a href="#truong-lien-thong" aria-controls="profile" role="tab" data-toggle="tab">                 <div class="row block-two-title">
                        <span class="block-two-link">Dự kiến điểm chuẩn</span>
                    </div></a></li>
                    <li role="presentation" class=" btn-view-all-tab"><a href="{{ route('allslug','tin-tuc-lien-thong')}}" class="btn-view-all">xem toàn bộ<i
                        class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                  </ul>
            
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="diem-chuan">
                        <div class=" row  block-two-ct3 m-0">
                            @foreach ($catediemchuan as $item)
                            @if ($loop->last)
                            <div class="col-md-6 pt-1 mobile-plr-10">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                            <img src="{{ $item->image}}" class="img-fluid" alt="{{ $item->title}}">
                                </a>
                                <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                                    <h2>{{$item->meta_title}}</h2>
                                </a>
                                <p>{{ $item->description}}</p>
                            </div>
                            @endif
                            @endforeach
                            <div class="col-md-6">
                                <div class="row">
                                    <ul class=" block-two-ct6-h2">
                                        @foreach ($catediemchuan as $item)
                                        @if ($loop->first)
                                        @endif
                                        @if($loop->remaining)
                                        <li class="pt-1">
                                            <div class="col-md-4 p-0">
                                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                            <img src="{{$item->image}}" class="img-fluid" alt="{{$item->title}}">
                                            </a>
                                            </div>
                                            <div class="col-md-8">
                                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                                <h2>{{ $item->meta_title}}</h2>
                                            </a>
                                            </div>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
        
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="truong-lien-thong">
                        <div class="block-two-ct3">
                            @foreach ($dkdiemchuan as $item)
                            <div class=" col-xs-12 col-md-3  col-sm-12">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                    <img src="{{ $item->image}}" class="img-fluid" alt="">
                                </a>
                                <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                                <h2>{{ $item->title}}</h2>
                                </a>
                            </div>
                            @endforeach
                            <div class="clear"></div>
                        </div>
                    </div>
                  </div>

                <div class="clear"></div>
            </div>
            <div class="mt-15">

                <ul class="nav nav-tab-home" role="tablist">
                    <li role="presentation" class="active"><a href="#tin-tuc-lien-thong" aria-controls="home" role="tab" data-toggle="tab">                <div class="row block-two-title">
                        <h2>Tin tức Liên thông</h2>
                    </div></a></li>
                    <li role="presentation"><a href="#truong-lien-thong2" aria-controls="profile" role="tab" data-toggle="tab">                 <div class="row block-two-title">
                        <span class="block-two-link">Các Trường Liên
                            Thông</span>
                    </div></a></li>
                    <li role="presentation" class=" btn-view-all-tab"><a href="{{ route('allslug','tin-tuc-lien-thong')}}" class="btn-view-all">xem toàn bộ<i
                        class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                  </ul>
            
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tin-tuc-lien-thong">
                        <div class="block-two-ct3">
                            @foreach ($tinlienthong as $item)
                            <div class=" col-xs-12 col-md-3  col-sm-12">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                    <img src="{{ $item->image}}" class="img-fluid" alt="">
                                </a>
                                <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                                <h2>{{ $item->title}}</h2>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="truong-lien-thong2">
                        <div class="col-md-12 block-two-ct3 block-two-ct3-tab block-two-ct3-tab-mobile">
                            @foreach ($truonglt as $item)
                            <div class="col-4 col-xs-12 col-md-2  col-sm-12">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                    <img src="{{ $item->image}}" class="img-fluid" alt="">
                                </a>
                                <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                                <h2>{{ $item->title}}</h2>
                                </a>
                            </div>
                            @endforeach
                            <div class="clear"></div>
                        </div>
                    </div>
                  </div>

                <div class="clear"></div>
            </div>

            <div class="mt-15">

                <ul class="nav nav-tab-home" role="tablist">
                    <li role="presentation" class="active"><a href="#khoi-thi" aria-controls="home" role="tab" data-toggle="tab">                <div class="row block-two-title">
                        <h2>Khối thi Đại học</h2>
                    </div></a></li>
                    <li role="presentation"><a href="#truong-lien-thong1" aria-controls="profile" role="tab" data-toggle="tab">                 <div class="row block-two-title">
                        <span class="block-two-link">Các Trường Liên
                            Thông</span>
                    </div></a></li>
                    <li role="presentation" class=" btn-view-all-tab"><a href="{{ route('allslug','khoi-thi')}}" class="btn-view-all">xem toàn bộ<i
                        class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                  </ul>
            
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="khoi-thi">
                        <div class="block-two-ct3">
                            @foreach ($khoithi as $item)
                            @if ($loop->last)
                            <div class="col-md-6 pt-1 mobile-plr-10">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                            <img src="{{ $item->image}}" class="img-fluid" alt="{{ $item->title}}">
                                </a>
                                <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                                    <h2>{{$item->title}}</h2>
                                </a>
                                <p>{!! $item->description !!}</p>
                            </div>
                            @endif
                            @endforeach
                            <div class="col-md-6">
                                <div class="row">
                                    <ul class=" block-two-ct6-h2">
                                        @foreach ($khoithi as $item)
                                        @if ($loop->first)
                                        @endif
                                        @if($loop->remaining)
                                        <li class="pt-1">
                                            <div class="col-md-4 p-0">
                                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                            <img src="{{$item->image}}" class="img-fluid" alt="{{$item->title}}">
                                            </a>
                                            </div>
                                            <div class="col-md-8">
                                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                                <h2>{{ $item->title}}</h2>
                                            </a>
                                            </div>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
        
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="truong-lien-thong1">
                        <div class="block-two-ct3">
                            @foreach ($tinlienthong as $item)
                            <div class=" col-xs-12 col-md-3  col-sm-12">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                    <img src="{{ $item->image}}" class="img-fluid" alt="">
                                </a>
                                <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                                <h2>{{ $item->title}}</h2>
                                </a>
                            </div>
                            @endforeach
                            <div class="clear"></div>
                        </div>
                    </div>
                  </div>

                <div class="clear"></div>
            </div>


        </div>
        <div class="w-32">
            <div class="siderbar-mobile">
            <div class="col-md-12 pt-md-3">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                    <h2 class="siderbar-1-title p-2">TUYỂN SINH ĐẠI HỌC</h2></span><span><a href="{{ route('allslug','dai-hoc')}}"
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="row m-0 ">
                            <ul class="pl-0">
                            @foreach ($tsdh as $item)
                            <li class="">
                                <div class="col-md-4 p-0">
                                <a href="{{ route('allslug',$item->slugs->slug)}}">
                                <img src="{{$item->image}}" class="img-fluid border-radius-5" alt="{{$item->title}}" max-height="69px">
                                    </a>
                                </div>
                                <div class="col-md-8 pr-0">
                                <a href="{{route('allslug',$item->slugs->slug)}}">
                                        <h2>{{$item->meta_title}}</h2>
                                    </a>
                                </div>
                            </li>
                            @endforeach
                        </ul>

                    </div>

                </div>
            </div>
            </div>
            
        <div class="siderbar-mobile">
            <div class="col-md-12 pt-md-3">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                        <h2 class="siderbar-1-title p-2">TUYỂN SINH CAO ĐẲNG</h2></span><span><a href="{{ route('allslug','cao-dang')}}"
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="row siderbar-cd">
                                                @foreach ($tscd as $item)
                        <div class="col-md-4 col-4 siderbar-child">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                            <img src="{{$item->image}}" class="img-fluid" alt="{{$item->title}}"  max-height="69px">
                                </a>
                                                            <a href="{{route('allslug',$item->slugs->slug)}}">
                                    <h2>{{$item->meta_title}}</h2>
                                </a>
                        </div>
                        @endforeach
                        </ul>

                    </div>

                </div>
            </div>
        </div>
            <div class="clear"></div>
            <div class="siderbar-mobile">
                        <div class="col-md-12 pt-md-3">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                    <h2 class="siderbar-1-title p-2">TUYỂN SINH TRUNG CẤP</h2></span><span><a href="{{ route('allslug','trung-cap')}}"
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="col-md-12 p-md-0">
                            <ul class="pl-0">
                            @foreach ($tstc as $item)
                            <li class="">
                                <div class="col-md-4 p-0">
                                <a href="{{ route('allslug',$item->slugs->slug)}}">
                                <img src="{{$item->image}}" class="img-fluid border-radius-5" alt="{{$item->title}}" max-height="69px">
                                    </a>
                                </div>
                                <div class="col-md-8 pr-0">
                                <a href="{{route('allslug',$item->slugs->slug)}}">
                                        <h2>{{$item->meta_title}}</h2>
                                    </a>
                                </div>
                            </li>
                            @endforeach
                        </ul>

                    </div>

                </div>
            </div>
        </div> 


        </div>
    </div>
</div>
<!-- end block two -->
<!-- block three -->
<div class="container block-three">
    <div class="row">
        <div class="w-68">
            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Tin giáo dục</h2><span><a href="{{ route('allslug','giao-duc-dao-tao') }}" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0 pt-md-2">
                    @foreach ($tingd as $item)
                        @if ($loop->first)
                        <div class="col-md-3">
                        <a href="{{ route('allslug',$item->slugs->slug)}}">
                        <img src="{{$item->image}}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                <h2 class="p-0 block-three-title-child">{{$item->title}}</h2>
                            </a>
                            <p>{!!$item->description!!}</p>
                        @endif
                        <ul class="pl-3 mb-0">
                        @if ($loop->remaining)
                            <li><a href="{{ route('allslug',$item->slugs->slug)}}">
                                    {{$item->title}}</a>
                            </li>
                        @endif
                    </ul>
                    @endforeach
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Thi THPT Vào 10</h2><span><a href="{{ route('allslug','tin-tuc-thpt')}}" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0  pt-md-2">
                    @foreach ($thpt as $item)
                        @if ($loop->first)
                        <div class="col-md-3">
                        <a href="{{ route('allslug',$item->slugs->slug)}}">
                        <img src="{{$item->image}}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                <h2 class="p-0 block-three-title-child">{{$item->title}}</h2>
                            </a>
                            <p>{!!$item->description !!}</p>
                        @endif
                        <ul class="pl-3 mb-0">
                        @if ($loop->remaining)
                            <li><a href="{{ route('allslug',$item->slugs->slug)}}">
                                    {{$item->title}}</a>
                            </li>
                        @endif
                    </ul>
                    @endforeach
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Chứng chỉ</h2><span><a href="{{ route('allslug','chung-chi')}}" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0  pt-md-2">
                    @foreach ($chungchi as $item)
                        @if ($loop->first)
                        <div class="col-md-3">
                        <a href="{{ route('allslug',$item->slugs->slug)}}">
                        <img src="{{$item->image}}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <a href="{{ route('allslug',$item->slugs->slug)}}">
                                <h2 class="p-0 block-three-title-child">{{$item->title}}</h2>
                            </a>
                            <p>{!! $item->description !!}</p>
                        @endif
                        <ul class="pl-3 mb-0">
                        @if ($loop->remaining)
                            <li><a href="{{ route('allslug',$item->slugs->slug)}}">
                                    {{$item->title}}</a>
                            </li>
                        @endif
                    </ul>
                    @endforeach
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Lao động - Du học</h2> <span class="block-two-link"></span><span><a href="{{route('allslug','lao-dong-du-hoc')}}" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3">
                    @foreach ($duhoc as $item)
                    <div class=" col-xs-12 col-md-3  col-sm-12">
                    <a href="{{ route('allslug',$item->slugs->slug)}}">
                    <img src="{{ $item->image}}" class="img-fluid" alt="">
                        </a>
                        <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                            <h2>{{$item->title}}</h2>
                        </a>
                    </div>
                    @endforeach

                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="w-32 block-three-right">
            <div class="col-md-12">

<!-- diendantuyensinh24h.com - 336x280 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-1895892504965300"
     data-ad-slot="6404403391"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

            </div>
            <div class="col-md-12 pt-2">
<!-- diendantuyensinh24h.com - 300x600 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-1895892504965300"
     data-ad-slot="3410095460"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
        </div>
    </div>
</div>
<!-- end block three -->
@endsection