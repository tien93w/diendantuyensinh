<?php $__env->startSection('meta'); ?>
<?php if($data && $meta): ?>
  <title>
    <?php echo e($data->meta_title != "" ? $data->meta_title : $data->title); ?>

  </title>
<meta name="robots" content="<?php echo e($data->index_seo === 1 ? 'index , follow' : 'noindex, nofollow'); ?>" />
  <meta name="description"
    content="<?php echo e($data->mdescription ? $data->mdescription : $meta->discription); ?>" />
  <meta property="og:image"
    content="<?php echo e($data->image != "" ? $data->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png'); ?>" />
  <meta name="keywords" content="<?php echo e($data->keywords ? $data->keywords : $meta->keysword); ?>" />
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('logo'); ?>
<p>
    <a href="<?php echo e(route('home')); ?>">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</p>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('fe/toc/bootstrap-toc.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('css/comment.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- content -->
      <div class="container pt-md-2 mt-md-2">
          <div class="row">
              <div class="w-70 page-content">
              <h1><?php echo e($data->title); ?></h1>
                  <div class="row page-text">
                      <div class="w-12 social-left">

                          <ul class="social-left-child">
                            
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                              <li class="page-comment"><a href="#wpcomm"><i class="fa fa-comments"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                      <div class="w-78 page-left">
                                            <div class="hidden-xs hidden-sm row col-12 page-ads">
                    <!-- diendantuyensinh24h - link -->
                    <ins class="adsbygoogle"
                        style="display:block;width:100%"
                        data-ad-client="ca-pub-1895892504965300"
                        data-ad-slot="4748493218"
                        data-ad-format="link"
                        data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                  </div>
                          <div class="col-md-12 block-one-left">
                              <?php if(count($link)>0): ?>
                              <ul>
                                  <?php $__currentLoopData = $link; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <li><a  href="<?php echo e($item->link); ?>" target="_blank" title="<?php echo e($item->title); ?>" alt="<?php echo e($item->alt); ?>"><?php echo e($item->title); ?></a></li>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </ul>
                              <?php endif; ?>
                          </div>
                  <div  id="page-text" class="col-md-12"><?php echo ads_in_post($data->content); ?></div>
                      </div>
                  </div>


                    
                    <?php echo $__env->make('include.likeshare', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                  <div class="">
                      <div class="row block-two-title">
                          <h2>TIN LIÊN QUAN</h2><span><a href="<?php echo e(route('allslug',$cate->slugs->slug)); ?>"
                                  class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                      aria-hidden="true"></i></a></span>
                      </div>
                      <div class="row d-flex m-0 block-two-ct3 block-two-ct32    mobile-img-post-bottom">
                        <?php $__currentLoopData = $datanews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <div class=" col-4 w-20 <?php echo e($k>5 ? " mobile-hidden" :""); ?>">
                          <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                          <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                            </a>
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                <h2><?php echo e($item->title); ?></h2>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <div class="clear"></div>
                      </div>
                  </div>
                                  
                <div class="">
                  <div class="col-md-12">
                    <div id="comments" class="comments-area">
                      <div id="respond" style="width: 0;height: 0;clear: both;margin: 0;padding: 0;"></div>
                      <div id="wc-comment-header">
                        <h3>
                          Bình Luận Của Bạn:

                        </h3>
                        <p>Nếu bạn có thắc mắc, ý kiến đóng góp của bạn xung quanh vấn đề này. Vui lòng điền thông tin
                          theo mẫu bên dưới rồi nhấn nút GỬI BÌNH LUẬN. Mọi ý kiến đóng góp đều được nhà trường đón đợi
                          và quan tâm. Những câu hỏi sẽ được các thầy cô trả lời và giải đáp trong thời gian sớm nhất
                        </p>

                      </div>
                      <div id="wpcomm" class="wpdiscuz_auth wpd-default">
                        <div class="wc-form-wrapper wc-main-form-wrapper" id="wc-main-form-wrapper-0_0">
                          <div class="wpdiscuz-comment-message" style="display: block;"></div>
                          <form class="wc_comm_form wc_main_comm_form" method="post" enctype="multipart/form-data"
                            action="<?php echo e(route('comment.add')); ?>">
                            <?php echo csrf_field(); ?>
                            <div class="wc-field-comment">
                              <div class="wpdiscuz-item wc-field-textarea">
                                <div class="wpdiscuz-textarea-wrap ">
                                  <div class="wc-field-avatararea" style="display: block;">
                                    <img alt="Giáo Viên Phụ trách"
                                      src="https://secure.gravatar.com/avatar/e5834b67a495187a3ee7893b2353e720?s=40&amp;d=mm&amp;r=g"
                                      srcset="https://secure.gravatar.com/avatar/e5834b67a495187a3ee7893b2353e720?s=80&amp;d=mm&amp;r=g 2x"
                                      class="avatar avatar-40 photo" height="40" width="40">
                                  </div>
                                  <textarea id="wc-textarea-0_0" placeholder="Tham gia thảo luận..." required=""
                                    id="body-form" data-toggle="collapse" data-target="#form-comment" name="body"
                                    class="wc_comment wpd-field"></textarea>
                                  <div class="autogrow-textarea-mirror"
                                    style="display: none; overflow-wrap: break-word; padding: 25px 78px 0px 85px; width: 595px; font-family: &quot;Helvetica Neue&quot;, Arial, Helvetica, sans-serif; font-size: 17px; line-height: 20px;">
                                    .<br>.</div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                            </div>
                            <div class="wc-form-footer" style="display: block;">
                              <div class="collapse wpd-form-row" id="form-comment">
                                <input type="hidden" value="<?php echo e(Auth::id()); ?>" name="user_id">
                                <input type="number" style="display:none" name="post_id" value="<?php echo e($data->id); ?>" />
                                <div class="wpd-form-col-left">
                                  <div class="wpdiscuz-item wc_name-wrapper wpd-has-icon">
                                    <div class="wpd-field-icon"><i class="fa fa-user"></i></div>
                                    <input
                                      value="<?php echo e(Auth::check() ? Auth::user()->name: old('name')); ?>"
                                      required="required" class="wc_name wpd-field" type="text" name="name"
                                      placeholder="Họ Và Tên*" maxlength="50" pattern=".{3,50}" title="">
                                    <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                        aria-hidden="true"></i><span>Nhập họ tên đầy đủ</span></div>
                                  </div>
                                  <div
                                    class="wpdiscuz-item custom_field_5ea2bc76c8f3f-wrapper wpd-has-icon wpd-has-desc">
                                    <div class="wpd-field-icon"><i style="opacity: 0.8;" class="fa fa-phone"></i></div>
                                    <input required="required"
                                      class="custom_field_5ea2bc76c8f3f wpd-field wpd-field-number" type="text"
                                      name="phone"
                                      value="<?php echo e(Auth::check() ? '0997654321': old('phone')); ?>"
                                      placeholder="Điện Thoại*">
                                    <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                        aria-hidden="true"></i><span>Điện thoại liên hệ</span></div>
                                  </div>
                                  <div class="wpdiscuz-item wc_email-wrapper wpd-has-icon">
                                    <div class="wpd-field-icon"><i class="fa fa-at"></i></div>
                                    <input
                                      value="<?php echo e(Auth::check() ? Auth::user()->email: old('email')); ?>"
                                      required="required" class="wc_email wpd-field" type="email" name="email"
                                      placeholder="Địa chỉ Email*">
                                    <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                        aria-hidden="true"></i><span>Thư điện tử</span></div>
                                  </div>
                                </div>
                                <div class="wpd-form-col-right">
                                  <div class="wc-field-submit">
                                    <label class="wpd_label" title="Stick this comment">
                                      <input id="wc_sticky_comment" class="wpd_label__checkbox" value="1"
                                        type="checkbox" name="wc_sticky_comment">
                                      <span class="wpd_label__text">
                                    </label>
                                    <input class="wc_comm_submit wc_not_clicked button alt" type="submit" name="submit"
                                      value="Gửi bình luận">
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                          </form>
                        </div>
                        <div id="wcThreadWrapper" class="wc-thread-wrapper">

                          <?php echo $__env->make('partials._comment_replies', ['comments' => $data->comments, 'post_id' => $data->id], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                          <div class="wpdiscuz-comment-pagination">
                            <div class="wc-load-more-submit-wrap">
                              <div class="wc-load-more-link" data-lastparentid="34319">
                                <?php echo e($data->comments->links()); ?>

                              </div>
                            </div>
                            <input id="wpdiscuzHasMoreComments" type="hidden" value="1">
                          </div>

                        </div>
                        <div class="wpdiscuz_clear"></div>
                      </div>
                    </div>
                  </div>
                </div>
                

              </div>
              <div class="w-30">

                  <?php echo $__env->make('fe.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
              </div>
          </div>
      </div>
  <!-- end content -->
<!--edit posst-->
     <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('post-edit')): ?>
  <div class="container-fluid" style="position: fixed;bottom:0px;padding:10px 0px;background-color:#cdcdcd">
    <a class="btn btn-secondary pull-right" href="<?php echo e(route('post.edit',$data->id)); ?>">Edit Post</a>
  </div>
  <?php endif; ?>

<div class="modal fade" id="myModal" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: none;
     border-radius: inherit;text-align: center">
      <div class="modal-body">
        <div class="row">
          <i class="fa fa-check-circle 2x center-block" style="font-size: 39px;color: #00A000" aria-hidden="true"></i>
        </div>
        <div class="row" style="color: #00A000">
          <?php if($message = Session::get('success')): ?>
            <h2><?php echo e($message); ?></h2>
          <?php endif; ?>

          <p>Để có thể chủ động hơn trong liên hệ với các thầy, cô. Bạn hãy like share và nhắn tin tại fanpage của nhà
            trường để được tư vấn nhiều hơn!</p>
        </div>
        <div class="btn-page-new">
          <span><a href="<?php echo e(route('home')); ?>" class="btn-resgister-page">Về Trang Chủ<i
                class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span><span><a href="#"
              class="btn-tu-van-page">Fanpage<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModaledit" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: none;
      border-radius: inherit;text-align: center">
      <div class="modal-body">
        <div class="row">
          <i class="fa fa-check-circle 2x center-block" style="font-size: 39px;color: #00A000" aria-hidden="true"></i>
        </div>
        <div class="row" style="color: #00A000">
          <?php if($message = Session::get('successedit')): ?>
            <h2><?php echo e($message); ?></h2>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('fe/toc/bootstrap-toc.min.js')); ?>"></script>
<script>
//   $(function() {
//   var $myToc = $('...');
//   Toc.init($myToc);
//   $('#page-text').scrollspy({
//     target: $myNav
//   });
// });
Toc.init({

// The element that the navigation will be created in
$nav: $('#toc'),

// The element where the search for headings will be limited to
$scope: $('#page-text')

});
</script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
  src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
  nonce="2wLpfQ0N"></script>
<?php if(session('success')): ?>
  <script>
    $(function () {
      $('#myModal').modal('show');
    });
  </script>
<?php endif; ?>
<?php if(session('successedit')): ?>
  <script>
    $(function () {
      $('#myModaledit').modal('show');
    });
  </script>
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.fe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/fe/post.blade.php ENDPATH**/ ?>