<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\PostLink;
class LinkComposer
{


/**
      * Bind data to the view.
      *
      * @param  View  $view
      * @return void
      */
    public function compose(View $view)
    {
        $view
        ->with('link', PostLink::all());
    }
}
