<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MenuComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'layouts.fe',
            'App\Http\ViewComposers\MenuComposer'
        );
        view()->composer(
            'fe.sidebar',
            'App\Http\ViewComposers\SideBarComposer'
        );
        view()->composer(
            '*',
            'App\Http\ViewComposers\MetaComposer'
        );
        view()->composer(
            '*',
            'App\Http\ViewComposers\LinkComposer'
        );
    }
}
