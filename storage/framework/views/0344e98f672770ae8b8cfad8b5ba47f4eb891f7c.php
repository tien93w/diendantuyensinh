<?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


   <div class="wc-comment wc-blog-guest wc_comment_level-1">
      <div class="wc-comment-left ">
         <div class="wpd-xborder"></div>
         <img alt="<?php echo e($comment->name); ?>"
            src="<?php echo e($comment->user_id != NULL ? $comment->user->image : asset('images/gavatar.png')); ?>"
            style="border: 1px solid #ddd;
      border-radius: 50%;
      box-shadow: none;padding:1px" height="64" width="64">
      </div>
      <div class="wc-comment-right">
         <div class="wc-comment-header">
            <div class="wc-comment-author ">
               <?php echo e($comment->user_id != NULL ? $comment->user->name : $comment->name); ?>

            </div>
            <div class="wpdiscuz_clear"></div>
         </div>
         <div class="wc-comment-text">
            <p><?php echo e($comment->body); ?></p>
         </div>
         <div class="wc-comment-footer">
            <div class="wc-footer-left">
               <span data-toggle="collapse" data-target="#commentdemo-<?php echo e($comment->id); ?>"
                  class="collapse wc-reply-button wc-cta-button" title="Trả lời"><i class="fa fa-comments"
                     aria-hidden="true"></i> Trả lời</span>

               <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('post-edit')): ?>
                  <span data-toggle="collapse" data-target="#commentedit-<?php echo e($comment->id); ?>"
                     class="collapse wc-reply-button wc-cta-button" title="Trả lời"><i class="fa fa-comments"
                        aria-hidden="true"></i>Sửa Comment</span>
                  <span><a href="<?php echo e(route('delcommentfront',$comment->id)); ?>"
                        data-toggle="tooltip" class="collapse wc-reply-button wc-cta-button"
                        data-original-title="Delete"
                        class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser"
                        title="XOA">
                        <i class="la la-close"></i>Xoa</a></span>
               <?php endif; ?>
            </div>
            <?php if(count($comment->replies) >0): ?>
               <div class="wc-footer-right">
                  <div class="wc-toggle pull-right" style="" aria-expanded="false" data-toggle="collapse"
                     data-target="#commentde-<?php echo e($comment->id); ?>" "><i class=" fa fa-chevron-down"
                     title="Hiện trả lời"></i></div>
               </div>
            <?php endif; ?>

            <div class="wpdiscuz_clear"></div>
         </div>
      </div>
      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('post-edit')): ?>
         <div class="collapse wc-form-wrapper wc-secondary-form-wrapper" id="commentedit-<?php echo e($comment->id); ?>" style="">
            <div class="wpdiscuz-comment-message" style="display: block;"></div>
            <div class="wc-secondary-forms-social-content"></div>
            <div class="clearfix"></div>

            <form class="wc_comm_form wc-secondary-form-wrapper" method="post" enctype="multipart/form-data"
               action="<?php echo e(route('comment.update',$comment->id)); ?>">
               <?php echo csrf_field(); ?>
               <?php echo e(method_field('PATCH')); ?>

               <div class="wc-field-comment">
                  <div class="wpdiscuz-item wc-field-textarea">
                     <div class="wpdiscuz-textarea-wrap ">
                        <div class="wc-field-avatararea">
                           <img alt="avatar" src="https://secure.gravatar.com/avatar/?s=48&amp;d=mm&amp;r=g"
                              srcset="https://secure.gravatar.com/avatar/?s=96&amp;d=mm&amp;r=g 2x"
                              class="avatar avatar-48 photo avatar-default" height="48" width="48">
                        </div>
                        <textarea placeholder="Tham gia thảo luận..." required="" name="body"
                           class="wc_comment wpd-field"
                           style="overflow: hidden; min-height: 2em; height: 60px;"><?php echo e($comment->body); ?></textarea>

                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="wc-form-footer" style="display: block;">
                  <div class="wpd-form-row">
                     <div class="wpd-form-col-left" style="">
                        <div class="wpdiscuz-item wc_name-wrapper wpd-has-icon">
                           <div class="wpd-field-icon"><i class="fa fa-user"></i></div>
                           <input value="<?php echo e($comment->name); ?>" required="required" class="wc_name wpd-field"
                              type="text" name="name" placeholder="Họ Và Tên*" maxlength="50" pattern=".{3,50}"
                              title="">
                           <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                 aria-hidden="true"></i><span>Nhập họ tên đầy đủ</span></div>
                        </div>
                     </div>
                     <div class="wpd-form-col-right">
                        <div class="wc-field-submit">
                           <input class="wc_comm_submit wc_not_clicked button alt" type="submit" name="submit"
                              value="Sửa bình luận">
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </form>
         </div>
         <div class="wpdiscuz_clear"></div>
      <?php endif; ?>
      <div class="wpdiscuz-comment-message"></div>
      <div class="collapse wc-form-wrapper wc-secondary-form-wrapper" id="commentdemo-<?php echo e($comment->id); ?>" style="">
         <div class="wpdiscuz-comment-message" style="display: block;"></div>
         <div class="wc-secondary-forms-social-content"></div>
         <div class="clearfix"></div>

         <form class="wc_comm_form wc-secondary-form-wrapper" method="post" enctype="multipart/form-data"
            action="<?php echo e(route('reply.add')); ?>">
            <?php echo csrf_field(); ?>
            <input type="hidden" name="post_id" value="<?php echo e($comment->post_id); ?>">
            <input type="hidden" name="comment_id" value="<?php echo e($comment->id); ?>">
            <div class="wc-field-comment">
               <div class="wpdiscuz-item wc-field-textarea">
                  <div class="wpdiscuz-textarea-wrap ">
                     <div class="wc-field-avatararea">
                        <img alt="avatar" src="https://secure.gravatar.com/avatar/?s=48&amp;d=mm&amp;r=g"
                           srcset="https://secure.gravatar.com/avatar/?s=96&amp;d=mm&amp;r=g 2x"
                           class="avatar avatar-48 photo avatar-default" height="48" width="48">
                     </div>
                     <textarea placeholder="Tham gia thảo luận..." required="" name="body" class="wc_comment wpd-field"
                        style="overflow: hidden; min-height: 2em; height: 60px;"></textarea>
                     <div class="autogrow-textarea-mirror"
                        style="display: none; overflow-wrap: break-word; padding: 10px; width: 495px; font-family: &quot;Helvetica Neue&quot;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px;">
                        .<br>.</div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="wc-form-footer" style="display: block;">
               <div class="wpd-form-row">
                  <div class="wpd-form-col-left"
                     style="<?php echo e(Auth::check() ? 'display:none':""); ?>">
                     <div class="wpdiscuz-item wc_name-wrapper wpd-has-icon">
                        <div class="wpd-field-icon"><i class="fa fa-user"></i></div>
                        <input
                           value="<?php echo e(Auth::check() ? Auth::user()->name: old('name')); ?>"
                           required="required" class="wc_name wpd-field"
                           type="<?php echo e(Auth::check() ? 'hidden':'text'); ?>"
                           name="name" placeholder="Họ Và Tên*" maxlength="50" pattern=".{3,50}" title="">
                        <div class="wpd-field-desc"><i class="fa fa-question-circle" aria-hidden="true"></i><span>Nhập
                              họ tên đầy đủ</span></div>
                     </div>
                     <div class="wpdiscuz-item custom_field_5ea2bc76c8f3f-wrapper wpd-has-icon wpd-has-desc">
                        <div class="wpd-field-icon"><i style="opacity: 0.8;" class="fa fa-phone"></i></div>
                        <input required="required" class="custom_field_5ea2bc76c8f3f wpd-field wpd-field-number"
                           type="<?php echo e(Auth::check() ? 'hidden':'text'); ?>"
                           name="phone"
                           value="<?php echo e(Auth::check() ? "0999384382" :old('phone')); ?>"
                           placeholder="Điện Thoại*">
                        <div class="wpd-field-desc"><i class="fa fa-question-circle" aria-hidden="true"></i><span>Điện
                              thoại liên hệ</span></div>
                     </div>
                     <div class="wpdiscuz-item wc_email-wrapper wpd-has-icon">
                        <div class="wpd-field-icon"><i class="fa fa-at"></i></div>
                        <input required="required" class="wc_email wpd-field" type="email"
                           value="<?php echo e(Auth::check() ? Auth::user()->email:old('email')); ?>"
                           name="email" placeholder="Địa chỉ Email*">
                        <div class="wpd-field-desc"><i class="fa fa-question-circle" aria-hidden="true"></i><span>Thư
                              điện tử</span></div>
                     </div>
                  </div>
                  <div class="wpd-form-col-right">
                     <div class="wc-field-submit">
                        <input class="wc_comm_submit wc_not_clicked button alt" type="submit" name="submit"
                           value="Gửi bình luận">
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div class="clearfix"></div>
         </form>
      </div>
      <div class="wpdiscuz_clear"></div>

   </div>
   <div id="commentde-<?php echo e($comment->id); ?>"
      class="collapse <?php echo e($k !=0 ? "":"in"); ?> wc-comment wc-reply wc-blog-user wc-blog-administrator wc_comment_level-2"
      style="" aria-expanded="">
      <?php echo $__env->make('partials._comment_replies', ['comments' => $comment->replies], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
   </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/partials/_comment_replies.blade.php ENDPATH**/ ?>