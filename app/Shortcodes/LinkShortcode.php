<?php
namespace App\Shortcodes;

class LinkShortcode {

  public function register($shortcode, $content, $compiler, $name, $viewData)
  {
    return '<a '. $shortcode->get('class', 'default') .'href="#forminpost" id="a-link-form-in-post">' . $content . '</a>';
  }
}