<?php ?>
@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Tạo mới link trong bài viết
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>

        </div>
        <div class="m-portlet__body">

            <!-- Display Validation Errors -->
            @include('admin.errors')

            <form class="form-horizontal" role="form" method="POST"
                action="{{ route('link.store') }}">
                {{ csrf_field() }}

                <div
                    class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Tiêu đề link</label>

                    <div class="col-md-12">
                        <input id="title" type="text" class="form-control" name="title"
                            value="{{ old('title') }}" required autofocus>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                    <label for="link" class="col-md-4 control-label">Link</label>

                    <div class="col-md-12">
                        <input id="link" type="text" class="form-control" name="link"
                            value="{{ old('link') }}">
                    </div>
                </div>
                <div
                        class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                    <label for="alt" class="col-md-4 control-label">Alt</label>

                    <div class="col-md-12">
                        <input id="alt" type="text" class="form-control" name="alt"
                               value="{{ old('alt') }}">
                    </div>
                </div>




                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Tạo mới
                        </button>

                        <a class="btn btn-link" href="{{ route('link.index') }}">
                            Hủy
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection