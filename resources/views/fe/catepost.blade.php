@section('meta')
@if($datapc && $meta)
    <title>{{ $datapc->title ? $datapc->title : "" }}</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description"
        content="{{ $datapc->description ? $datapc->description : $meta->discription }}" />
    <meta property="og:image"
        content="{{ $datapc->image != "" ? $datapc->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png' }}" />
    <meta name="keywords"
        content="{{ $datapc->keywords ? $datapc->keywords : $meta->keysword }}" />
@endif
@endsection
@section('logo')
<p>
    <a href="{{ route('home')}}">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</p>
@endsection
@extends('layouts.fe')
@section('content')
    <!-- content -->
    <div class="container p-md-2 mt-md-2">
        <div class="row {{Request::has('page') && Request::get('page') > 1 ? 'pt-md-3' : ''}}">
            <div class="w-70 page-content">
            @if (Request::has('page') && Request::get('page') > 1)
            <div class="">
                            <div class=" row page-text page-category">
                                      <div class=" w-12 social-left">
                          <ul class="social-left-child">
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                <div class="w-78">
                    @if(count($datas)>0)
                    @foreach($datas as $k=>$data)
                <div class="row pb-2 m-md-0 page-category-child" style="{{ $k!=0 ? 'padding: 1rem 0rem;
                border-top: dashed 1px #e1e1e1;margin-left:0px;    padding-left: 10px;
    padding-right: 10px;' : 'padding-top: 70px;    padding-left: 10px;
    padding-right: 10px;'}}">
                                    <div class="col-md-3 col-5">
                                        <div class="">
                                        <a
                                        href="{{ route('allslug', $data->slugs->slug) }}">
                                        <img src="{{ $data->image }}" class="img-fluid">
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-7 mobile-pl-0">
                                        <div class="">
                                                                               <h2><a class="title-news "
                                        href="{{ route('allslug', $data->slugs->slug) }}">{{ $data->title }}</a>
                                        </h2>
                                                                            <p class="mb-0 mobile-hidden">{!! html_cut($data->description, 281)!!}</p>
                                        </div>
                                    </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p>Không tìm thấy bài viết</p>
                        </div>
                    </div>
                @endif
                                {{ $datas->render('fe.link') }}
                </div>
            </div>
            @else
            <h1>{{ $datapc->title }}</h1>
            <div class="col-md-12 cates-top-5">
                <div class="row cates-top-5-row">
                    <div class="col-md-6 cates-top-5-left pr-md-0">
                            @foreach ($datas as $k=>$data)
                                @if ($k == 0)
                                    <a
                                        href="{{ route('allslug', $data->slugs->slug) }}">
                                        <img src="{{ $data->image }}" class="img-fluid">
                                    </a>
                                    <h2><a class="title-news "
                                        href="{{ route('allslug', $data->slugs->slug) }}">{{ $data->title }}</a>
                                    </h2>
                                    <p class="mb-0">{!! html_cut($data->description, 200)!!}</p>
                                @endif
                            @endforeach
                    </div>
                    <div class="col-md-6 cates-top-5-right">
                        <div class="row">
                                @foreach ($datas as $k=>$data)
                                    @if ($k> 0 & $k<5)
                                    <div class="col-md-6 col-12">
                                    <div class="row">
                                    <div class="col-md-12 col-4">
                                        <div class="">
                                        <a
                                        href="{{ route('allslug', $data->slugs->slug) }}">
                                        <img src="{{ $data->image }}" class="img-fluid">
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-8 mobile-pl-0">
                                        <div class="">
                                                                               <h2><a class="title-news "
                                        href="{{ route('allslug', $data->slugs->slug) }}">{{ $data->title }}</a>
                                        </h2> 
                                        </div>
                                    </div>

                                    </div>
                        </div>
                                    @endif
                                 @endforeach
                        </div>
                    </div>
                </div>
            <div class=" row page-text page-category pt-md-2">
                                      <div class="w-12 social-left">
                          <ul class="social-left-child">
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                <div class="w-78 pt-0">
                    @if(count($datas)>0)
                    @foreach($datas as $k=>$data)
                    @if ($k>4)
                    <div class="row m-md-0 pb-2 page-category-child" style="{{ $k!=5 ? 'padding: 1rem 0rem;border-top: dashed 1px #e1e1e1;' : 'padding-top:10px'}}">
                                    <div class="col-md-3 col-5">
                                        <div class="">
                                        <a
                                        href="{{ route('allslug', $data->slugs->slug) }}">
                                        <img src="{{ $data->image }}" class="img-fluid">
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-7 mobile-pl-0">
                                        <div class="">
                                                                               <h2><a class="title-news "
                                        href="{{ route('allslug', $data->slugs->slug) }}">{{ $data->title }}</a>
                                        </h2>
                                                                            <p class="mb-0 mobile-hidden">{!! html_cut($data->description, 200)!!}</p>
                                        </div>
                                    </div>
                            </div>
                            <div class="clear"></div>
                    @endif

                    @endforeach
                @else
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p>Không tìm thấy bài viết</p>
                        </div>
                    </div>
                @endif
                {{ $datas->render('fe.link') }}
                <!--{{ $datas->onEachSide(5)->links() }}-->
                </div>
            </div>
            @endif
            </div>
            </div>
            <div class="w-30">
                @include('fe.sidebar')
            </div>
        </div>
    </div>
<!-- end content -->

@endsection
@section('js')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
    nonce="2wLpfQ0N">
</script>
@endsection
