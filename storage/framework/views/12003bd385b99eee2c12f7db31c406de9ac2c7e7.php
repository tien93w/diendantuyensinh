<?php $__env->startSection('meta'); ?>
<?php if($data && $meta): ?>
  <title>
    <?php echo e($data->meta_title != "" ? $data->meta_title : $data->title); ?>

  </title>
  <meta name="robots" content="noindex, nofollow" />
  <meta name="description"
    content="<?php echo e($data->mdescription ? $data->mdescription : $meta->discription); ?>" />
  <meta property="og:image"
    content="<?php echo e($data->image != "" ? $data->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png'); ?>" />
  <meta name="keywords" content="<?php echo e($data->keywords ? $data->keywords : $meta->keysword); ?>" />
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('home'); ?>
<p id="title">
  <a href="<?php echo e(route('home')); ?>"></a>
</p>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="" id="inner">
  <div class="wrap">
    <div id="content-category-wrap " class="top-p20">
      <div id="content" class="container">
        <div class="page-content">
          <div class="col-md-12 no-padding">
            <h1 class="categoty-title"><?php echo e($data->title); ?></h1>
          </div>
          <div class="fb-like" data-href="<?php echo e(Request::url()); ?>" data-width="" data-layout="standard"
            data-action="like" data-size="large" data-share="true"></div>
          <div class="col-md-12 top-p20">
            <div class="row">
              <div class="clear" data-sticky-container></div>
              <div class="col-md-1 ">
                <div class="fixed-socialdiv">
                  <ul class="dt-news__social" data-margin-top="52" data-margin-bottom="16"
                    style="position: fixed; width: 80px; left: 152px; top: 286px;">
                    <li>

                      <a target="_blank" title="Chia sẻ lên Facebook"
                        href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(Request::url()); ?>"
                        class="dt-social__item dt-social__item--facebook">
                        <i class="dt-icon icon-facebook"></i>
                      </a>
                    </li>
                    <!--<li>-->
                    <!--  <a title="Chia sẻ qua Email" class="dt-social__item" href="mailto:?subject=/the-gioi/nga-cao-buoc-nha-khoa-hoc-phan-quoc-vi-tuon-bi-mat-cho-trung-quoc-20200616104340078.htm">-->
                    <!--    <i class="dt-icon icon-email"></i>-->
                    <!--  </a>-->
                    <!--</li>-->
                    <li>
                      <a title="Bình luận" class="dt-social__item dt-social__item--primary" href="#comment-post">
                        <i class="dt-icon icon-comment"></i>
                      </a>
                    </li>
                  </ul>
                </div>

              </div>
              <div class="col-md-11">
                <div class="post-content-text">
                  <div class="content-page-description">
                    <strong><?php echo $data->description; ?></strong>
                  </div>
                  <?php echo $data->content; ?>

                </div>



                



              </div>

            </div>
          </div>




        </div>
        
        
        <div id="positionneo"></div>
      </div>




    </div>
  </div>
</div>




<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
  src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
  nonce="2wLpfQ0N"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.fe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/fe/page.blade.php ENDPATH**/ ?>