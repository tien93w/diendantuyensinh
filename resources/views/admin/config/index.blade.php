@extends('layouts.admin')

@section('content')

<div class="container">
    <form action="{{ route('site.store') }}" method="POST">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="" class="control-label">Tiêu đề trang</label>
            <input type="text" class="form-control" name="title"
                value="{{ isset($datas) ? $datas->title :null }}">
        </div>
        <div class="form-group">
            <label for="" class="control-label">Mô tả trang</label>
            <textarea name="discription" id="" class="form-control"
                rows="4">{!! isset($datas) ? $datas->discription :null !!}</textarea>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Từ khoá trang</label>
            <textarea name="keysword" id="" class="form-control"
                rows="4">{!! isset($datas) ? $datas->keysword :null !!}</textarea>
        </div>
        {{-- <div class="row">
            <div class="col-md-6">
                <div class="form-group" >
                    <label for="image" class="control-label">Logo trang</label><br>
                        <span class="form-group-btn">
                        <a id="lfm" data-input="image" data-preview="holder1" class="btn btn-primary text-white">
                            <i class="fa fa-picture-o"></i> Chọn
                        </a>
                        </span>
                        
                        <div class="col-md-3">
                            <div id="holder1" class="admin-logo" style="margin-top:15px;height:100px;">
                                <img src="{!!  isset($datas) ? $datas->logo :null !!}" alt="">
                            </div>
                        </div>
                        <input id="image" style="opacity:0" class="form-control" type="text" name="image" value="{!!   isset($datas) ? $datas->logo :null !!}">
                        
                    </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" >
                    <label for="favicon" class="control-label">Favicon</label><br>
                        <span class="form-group-btn">
                        <a id="favicon" data-input="faviconc" data-preview="faviconv" class="btn btn-primary text-white">
                            <i class="fa fa-picture-o"></i> Chọn
                        </a>
                        </span>
                        
                        <div class="col-md-3">
                            <div id="faviconv" class="admin-logo" style="margin-top:15px;height:100px;">
                                <img src="{!!  isset($datas) ? $datas->favicon :null !!}" alt="">
                            </div>
                        </div>
                        <input id="faviconc" class="form-control" style="opacity:0"  type="text" name="favicon" value="{!!  isset($datas) ? $datas->favicon :null !!}">
                        <style>
                            .admin-logo image{
                                max-height: 100px !important;
                            }
                            #faviconv img{
                                width: 100%;
                            }
                            #holder1 img{
                                width: 100%;
                            }
                        </style>
                    </div>
            </div>

        </div> --}}
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button type="submit" class="btn btn-primary">
                    Lưu lại
                </button>

                <a class="btn btn-link" href="{{ route('site.create') }}">
                    Hủy
                </a>
            </div>
        </div>
    </form>
</div>
<script src="http://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>


<script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script type="text/javascript">
    $('#lfm').filemanager('image');
    $('#favicon').filemanager('image');
</script>
@endsection