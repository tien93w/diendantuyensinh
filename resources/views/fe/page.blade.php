@section('meta')
@if($data && $meta)
  <title>
    {{ $data->meta_title != "" ? $data->meta_title : $data->title }}
  </title>
  <meta name="robots" content="noindex, nofollow" />
  <meta name="description"
    content="{{ $data->mdescription ? $data->mdescription : $meta->discription }}" />
  <meta property="og:image"
    content="{{ $data->image != "" ? $data->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png' }}" />
  <meta name="keywords" content="{{ $data->keywords ? $data->keywords : $meta->keysword }}" />
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
  <a href="{{ route('home') }}"></a>
</p>
@endsection
@section('content')

<div class="" id="inner">
  <div class="wrap">
    <div id="content-category-wrap " class="top-p20">
      <div id="content" class="container">
        <div class="page-content">
          <div class="col-md-12 no-padding">
            <h1 class="categoty-title">{{ $data->title }}</h1>
          </div>
          <div class="fb-like" data-href="{{ Request::url() }}" data-width="" data-layout="standard"
            data-action="like" data-size="large" data-share="true"></div>
          <div class="col-md-12 top-p20">
            <div class="row">
              <div class="clear" data-sticky-container></div>
              <div class="col-md-1 ">
                <div class="fixed-socialdiv">
                  <ul class="dt-news__social" data-margin-top="52" data-margin-bottom="16"
                    style="position: fixed; width: 80px; left: 152px; top: 286px;">
                    <li>

                      <a target="_blank" title="Chia sẻ lên Facebook"
                        href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}"
                        class="dt-social__item dt-social__item--facebook">
                        <i class="dt-icon icon-facebook"></i>
                      </a>
                    </li>
                    <!--<li>-->
                    <!--  <a title="Chia sẻ qua Email" class="dt-social__item" href="mailto:?subject=/the-gioi/nga-cao-buoc-nha-khoa-hoc-phan-quoc-vi-tuon-bi-mat-cho-trung-quoc-20200616104340078.htm">-->
                    <!--    <i class="dt-icon icon-email"></i>-->
                    <!--  </a>-->
                    <!--</li>-->
                    <li>
                      <a title="Bình luận" class="dt-social__item dt-social__item--primary" href="#comment-post">
                        <i class="dt-icon icon-comment"></i>
                      </a>
                    </li>
                  </ul>
                </div>

              </div>
              <div class="col-md-11">
                <div class="post-content-text">
                  <div class="content-page-description">
                    <strong>{!! $data->description!!}</strong>
                  </div>
                  {!! $data->content !!}
                </div>



                {{-- <div class="row section-ulnew">
                  <div class="col-md-12 header-relate-post">
                    <h2>TIN MỚI</h2>
                  </div>
                  <div class="col-md-12">
                    <ul style="margin-left: 0.5%;list-style:none" class="post-cung-chuyenmuc">
                      @foreach($datanews as $item)
                        <li>
                          <a href="{{ route('allslug',$item->slugs->slug) }}"
                            title="{{ $item->title }}">{{ ucwords($item->title) }}</a>
                        </li>

                      @endforeach

                    </ul>
                  </div>

                </div> --}}



              </div>

            </div>
          </div>




        </div>
        {{-- end-md 9 --}}
        {{-- <div class="siderbar-content">
          @include('fe.sidebar')

        </div> --}}
        <div id="positionneo"></div>
      </div>




    </div>
  </div>
</div>




@endsection
@section('js')

<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
  src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
  nonce="2wLpfQ0N"></script>

@endsection
