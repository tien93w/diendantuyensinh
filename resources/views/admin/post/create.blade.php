<?php ?>

@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Tạo bài viết
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>

        </div>
        <div class="m-portlet__body">

            @include('admin.errors')
            <form class="form-horizontal" style="width:100%" role="form" method="POST"
                action="{{ route('post.store') }}">

                <div class="row">

                    {{ csrf_field() }}
                    <div class="col-md-9">


                        <div
                            class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-12 control-label">Tên bài viết</label>

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title"
                                    value="{{ old('title') }}" onkeyup="ChangeToSlug()" required
                                    autofocus>

                                @if($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div
                            class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                            <label for="slug" class="col-md-12 control-label">Slug bài viết</label>

                            <div class="col-md-12">
                                <input id="slug" type="text" class="form-control" name="slug"
                                    value="{{ old('slug') }}">

                                @if($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12 video-postcustom" style="display: none">
                            <div class="form-group">
                                <label for="video">Video Đại Diện</label>
                                <textarea name="video" class="form-control" id="video" rows="4"></textarea>
                            </div>
                        </div>
                        {{-- <div class="form-group col-md-12">
                            <label for="">Danh muc</label>
                            <select name="cid" class="form-control">
                                {{ old('cid') }}
                                <option value="">Chọn</option>
                                <?php categoryParent($data,$parent = 0, $str="", $select =old('cid'))?>
                            </select>
                        </div> --}}

                        <div
                            class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label">Mô tả ngắn</label>

                            <div class="col-md-12">
                                <textarea rows="7" id="description" name="description"
                                    class="form-control my-editor">{{ old('description') }}</textarea>
                                @if($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div
                            class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-12 control-label">Nội dung</label>

                            <div class="col-md-12">
                                <textarea id="content" rows="40" name="content"
                                    class="form-control my-editor">{{ old('content') }}</textarea>
                                @if($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('tag', 'Tags', ['class' => 'control-label']) !!}
                                {!! Form::select('tag[]', $tags, old('tag'), ['class' => 'form-control select2',
                                'multiple' => 'multiple', 'id' => 'selectall-tag' ]) !!}
                                <p class="help-block"></p>
                                @if($errors->has('tag'))
                                    <p class="help-block">
                                        {{ $errors->first('tag') }}
                                    </p>
                                @endif
                                <div style="color: #0E0EFF;padding-bottom: 30px">
                                    <a style="background-color: #fdf7f7" type="button" class="btn  btn-xs"
                                        id="selectbtn-tag">
                                        Chọn tất cả tag
                                    </a>
                                    <a style="background-color: #fdf7f7" type="button" class="btn btn-xs"
                                        id="deselectbtn-tag">
                                        Xoá tất cả tag
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 link-postcustom" style="display: none">
                            <h2>Linh liên kết</h2>
                            <div class="form-group">
                                <label for="">Linh liên kết 1</label>
                                <input type="text" class="form-control" name="link1" placeholder="Nhap link">
                            </div>
                            <div class="form-group">
                                <label for="">Linh liên kết 2</label>
                                <input type="text" class="form-control" name="link2" placeholder="Nhap link">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2>SEO
                                <span style="font-size: 40px; float: right" class="" data-toggle="collapse"
                                    data-target="#demo"><i class="fa fa-angle-double-down"
                                        style="font-size: 40px;color: #0b51c5" aria-hidden="true"></i></span>
                            </h2>
                        </div>
                        <div id="demo" class="collapse">


                            <div
                                class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
                                <label for="meta_title" class="col-md-12 control-label">Meta Title</label>

                                <div class="col-md-12">
                                    <div id="ameta_title"></div>

                                    <input rows="7" id="meta_title" name="meta_title" class="form-control"
                                        value="{{ old('meta_title') }}"/>
                                    @if($errors->has('meta_title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('meta_title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div
                                class="form-group{{ $errors->has('keywords') ? ' has-error' : '' }}">
                                <label for="keywords" class="col-md-12 control-label">Meta Keywords</label>

                                <div class="col-md-12">
                                    <div id="akeywords"></div>
                                    <input rows="7" id="keywords" name="keywords" class="form-control"
                                        value="{{ old('keywords') }}"/>
                                    @if($errors->has('keywords'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('keywords') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div
                                class="form-group{{ $errors->has('mdescription') ? ' has-error' : '' }}">
                                <label for="mdescription" class="col-md-12 control-label">Meta Description</label>

                                <div class="col-md-12">
                                    <div id="amdescription"></div>
                                    <textarea rows="4" id="mdescription" name="mdescription"
                                        class="form-control">{{ old('mdescription') }}</textarea>
                                    @if($errors->has('mdescription'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mdescription') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-12" style="padding-top: 30px">
                                <button type="submit" class="btn btn-primary">
                                    Tạo mới
                                </button>

                                <a class="btn btn-link" href="{{ route('post.index') }}">
                                    Hủy
                                </a>
                            </div>
                        </div>


                    </div>
                    {{-- end md9 --}}
                    <div class="col-md-3">
                        <div class="form-group row" style="margin:0px">
                            <div class="col-md-12">
                                <div class="m-form__group form-group row">
                                    <label class="col-12 col-form-label">Bài viết nổi bật</label>
                                    <div class="col-12">
                                                    <span class="m-switch m-switch--success">
                                                        <label>
                                                         <input type="checkbox"  checked  value="1" name="feature">

                                                        <span></span>
                                                        </label>
                                                    </span>
                                    </div>
                                    <label class="col-12 col-form-label">Public bài viết</label>
                                    <div class="col-12">
                                                    <span class="m-switch m-switch--warning">
                                                        <label>
                                                         <input type="checkbox"  checked  value="1" name="public">
                                                        <span></span>
                                                        </label>
                                                    </span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12">
                                <h2>Thời gian public</h2>
                                <div class="form-group">
                                    <input type="datetime-local" class="form-control" name="published_at" value="{{ old('published_at') ? date('Y-m-d\Th:m:s',  old('published_at')) : ""     }}" placeholder="">
                                </div>
                            </div>
                            
                                    <div class="col-md-12">
                                        <h3>Post template</h3>
                                        <select name="type" id="type" onchange="pageType(this)" class="form-control">
                                            <option
                                                {{ old('page-type') == 'new' ? 'selected' :'' }}
                                                value="new">Default</option>
                                            <option
                                                {{ old('page-type') == 'landing' ? 'selected' :'' }}
                                                value="landing">Landing</option>
                                            <option
                                                {{ old('page-type') == 'newcustom' ? 'selected' :'' }}
                                                value="newcustom">Custom Post</option>
                                        </select>
                                    </div>

                                    <div class="col-md-12">
                                        {!! Form::label('cate', 'Category'.'', ['class' => 'control-label']) !!}
                                        <select class="form-control select2" name="cate[]" multiple="multiple">
                                            <?php categoryParentSelect2($data ,$parent = 0, $str="",old('cate') ? old('cate') : []); ?>
                                        </select>
                                        <p class="help-block"></p>
                                        @if($errors->has('cate'))
                                            <p class="help-block">
                                                {{ $errors->first('cate') }}
                                            </p>
                                        @endif
                                    </div>
                            <div class="col-md-12">
                                <label for="image" class="control-label">Ảnh đại diện</label><br>
                                <span class="form-group-btn">
                                    <a id="lfm" data-input="image" data-preview="holder"
                                        class="btn btn-primary text-white">
                                        <i class="fa fa-picture-o"></i> Chọn
                                    </a>
                                </span>
                                <input id="image" class="form-control" type="text" name="image"
                                    value="{{ old('image') }}">
                                <div id="holder" style="margin-top:15px;max-height:300px;"></div>
                            </div>

                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.4.1/tinymce.min.js" integrity="sha512-c46AnRoKXNp7Sux2K56XDjljfI5Om/v1DvPt7iRaOEPU5X+KZt8cxzN3fFzemYC6WCZRhmpSlZvPA1pttfO9DQ==" crossorigin="anonymous"></script>
<script src="{{ asset('/admin/js/tiny.js') }}"></script>
<script src="{{ asset('/admin/js/slug.js') }}"></script>
<script type="text/javascript">
    function pageType(obj) {
        var value = obj.value;
        if (value === 'newcustom') {
            $('.link-postcustom').css({
                'display': 'block'
            })
        }
        if (value === 'post') {
            $('.link-postcustom').css({
                'display': 'none'
            })
        }
        if (value === 'newcustom') {
            $('.video-postcustom').css({
                'display': 'block'
            })
        }
        if (value === 'post') {
            $('.video-postcustom').css({
                'display': 'none'
            })
        }
    }
    $('#lfm').filemanager('image');
</script>
<script>
    $("#selectbtn-tag").click(function () {
        $("#selectall-tag > option").prop("selected", "selected");
        $("#selectall-tag").trigger("change");
    });
    $("#deselectbtn-tag").click(function () {
        $("#selectall-tag > option").prop("selected", "");
        $("#selectall-tag").trigger("change");
    });

    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
<script>
    $('#meta_title').keyup(function () {
        $('#ameta_title').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
    $('#keywords').keyup(function () {
        $('#akeywords').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
    $('#mdescription').keyup(function () {
        $('#amdescription').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
</script>
</div>


@endsection