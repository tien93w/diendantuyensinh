<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Slug\SlugRepositoryInterface;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Tag\TagRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DataTables;
use App\Models\Post;

class PostController extends Controller
{
    protected $repoPost;
    protected $repoSlug;
    protected $repoCate;
    protected $repoTag;
    public function __construct(
        TagRepositoryInterface $repoTag,
        PostRepositoryInterface $repoPost,
        SlugRepositoryInterface $repoSlug,
        CategoryRepositoryInterface $repoCate)
    {
        $this->repoPost = $repoPost;
        $this->repoSlug = $repoSlug;
        $this->repoCate = $repoCate;
        $this->repoTag = $repoTag;
        $this->middleware('permission:post-list|post-create|post-edit|post-delete', ['only' => ['index','store']]);
        $this->middleware('permission:post-create', ['only' => ['create','store']]);
        $this->middleware('permission:post-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:post-delete', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if(!empty($request->filter_gender))
            {   $datass = $request->filter_gender;
                $data = $this->repoPost->whereHas('cates', function ($query) use($datass){
                    $query->where('cate_id',$datass);
                })->with('slugs')->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a  href="'.route('post.edit', $row->id).'" data-toggle="tooltip" target="_blank"   data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                        <i class="la la-edit"></i></a> <a  href="'.route('allslug', $row->slugs->slug).'" target="_blank" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="View" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                        <i class="la la-eye"></i></a><a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                        <i class="la la-close"></i></a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }else{
                $data = $this->repoPost->with('slugs')->orderBy('id','DESC')->get();
                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a  href="'.route('post.edit', $row->id).'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                    <i class="la la-edit"></i></a> <a  href="'.route('allslug', $row->slugs->slug).'" target="_blank" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="View" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                    <i class="la la-eye"></i></a><a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                    <i class="la la-close"></i></a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
                
            }

        }
        $datas= $this->repoCate->all();
        return view('admin.post.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags  = $this->repoTag->pluck('name','id');
        $data= $this->repoCate->all();
        
        return view('admin.post.create',compact('data','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:slugs',
            'description' => 'required',
            'image'=>'required',
            'content'=>'required',

        ]);
        $inputs = $request->except(['_token','slug','type']);
        $idSlugs = $this->repoSlug->insertGetId($request->only(['slug','type']));
        $inputs['slug_id'] = $idSlugs;
        $inputs['uid'] = Auth::id();
        $request->index_seo == NULL ? ($inputs['index_seo'] = 0) : ($inputs['index_seo'] = 1);
        $request->public == NULL ? ($inputs['public'] = 0) : ($inputs['public'] = 1);
        $request->feature == NULL ? ($inputs['feature'] = 0) : ($inputs['feature'] = 1);
        $data = $this->repoPost->create($inputs);
        $data->tag()->sync($request->input('tag'));
        $data->cates()->sync((array)$request->input('cate'));
        if($data->wasRecentlyCreated === false){
            $this->repoSlug->delete($idSlugs);
        }else{
            return redirect()->route('post.index')
            ->with('success','Tạo mới bài viết thành công');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data2 = $this->repoPost->find($id);
        $tags = $this->repoTag->pluck('name', 'id');
        $data = $this->repoCate->all();
        return view('admin.post.edit',compact('data2','data','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //dd($request->published_at);
        $data = $this->repoPost->find($id);
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:slugs,slug,'.$data->slug_id ,
            'description' => 'required',
            'image'=>'required',
            'content'=>'required',
            'cate.*' => 'exists:category_posts,id',
            'tag.*' => 'exists:tags,id',
        ]);
        $inputs = $request->except('slug','_token','_method','tag','type','cate');
        $request->index_seo == NULL ? ($inputs['index_seo'] = 0) : ($inputs['index_seo'] = 1);
        $request->public == NULL ? ($inputs['public'] = 0) : ($inputs['public'] = 1);
        $request->feature == NULL ? ($inputs['feature'] = 0) : ($inputs['feature'] = 1);
        //dd($inputs);
        $this->repoPost->update($id,$inputs);
        $this->repoSlug->update($data->slug_id,['slug'=>$request->slug,'type'=>$request->type]);
        $data->tag()->sync((array)$request->input('tag'));
        $data->cates()->sync((array)$request->input('cate'));
        return redirect()->back()
            ->with('success','Sửa bài viết thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repoPost->deleteWithSlug($id);
        return redirect()->route('post.index')
            ->with('success','Xóa bài viết thành công');
    }
}
