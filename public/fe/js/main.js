document.addEventListener("DOMContentLoaded", function () {
    var lazyloadImages = document.querySelectorAll("img.lazy");
    var lazyloadThrottleTimeout;

    function lazyload() {
        if (lazyloadThrottleTimeout) {
            clearTimeout(lazyloadThrottleTimeout);
        }

        lazyloadThrottleTimeout = setTimeout(function () {
            var scrollTop = window.pageYOffset;
            lazyloadImages.forEach(function (img) {
                if (img.offsetTop < (window.innerHeight + scrollTop)) {
                    img.src = img.dataset.src;
                    img.classList.remove('lazy');
                }
            });
            if (lazyloadImages.length == 0) {
                document.removeEventListener("scroll", lazyload);
                window.removeEventListener("resize", lazyload);
                window.removeEventListener("orientationChange", lazyload);
            }
        }, 20);
    }

    document.addEventListener("scroll", lazyload);
    window.addEventListener("resize", lazyload);
    window.addEventListener("orientationChange", lazyload);
});
$(window).scroll(function (e) {
    if ($("#inner").width() > 768) {
        var elmnt = $(".page-content").height() - 300;
        if ($(".page-content").height() > $(".siderbar-content").height()) {
            $(".siderbar-content").height(elmnt);
        }


    }
});
$(document).ready(function () {
    $('.page-content').bind('cut copy paste', function (e) {
        e.preventDefault();
    });

    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            console.log('You cannot inspect Element');
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            console.log('You cannot inspect Element');
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            console.log('You cannot inspect Element');
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            console.log('You cannot inspect Element');
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            console.log('You cannot inspect Element');
            return false;
        }

    }

});
// $(document).bind("contextmenu",function(e) {
//  e.preventDefault();
// });

$(window).scroll(function (e) {
    var $el = $('.dt-news__social');
    var isPositionFixed = ($el.css('position') == 'fixed');
    if ($(this).scrollTop() > 200 && !isPositionFixed) {
        $el.css({
            'position': 'fixed',
            'top': '10px'
        });
    }
    if ($(this).scrollTop() < 200 && isPositionFixed) {
        $el.css({
            'position': 'static',
            'top': '10px'
        });
    }
});