<?php ?>


<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="col-md-12">
        <?php if($message = Session::get('success')): ?>
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert"></button>
                <strong><?php echo e($message); ?></strong>
            </div>
        <?php endif; ?>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Thông tin đăng ký
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>

        </div>
        <div class="m-portlet__body">

            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            </div>
            <!--end: Search Form -->

            <!--begin: Datatable -->
            <div class="m_datatable" id="child_data_ajax">

                <table class="data-table m-datatable__table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Ngày</th>
                            <th>Tên Học Viên</th>
                            <th>Số điện thoại</th>
                            <th>Số phụ huynh</th>
                            <th width="100px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <!--end: Datatable -->
        </div>
    </div>

    <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var table = $('.data-table').DataTable({
                    processing: true,
                    dom: 'Bfrtip',
                    serverSide: true,
                    buttons: [
                        'excel', 'pdf'
                    ],
                    ajax: "<?php echo e(route('reg.index')); ?>",
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex'
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'student',
                            name: 'student'
                        },
                        {
                            data: 'phone',
                            name: 'phone'
                        },
                        {
                            data: 'parent_phone',
                            name: 'parent_phone'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ]
                });

                $('body').on('click', '.deleteUser', function () {
                    //$(".deleteUser").click(function(){

                    var page_id = $(this).data("id");
                    var r = confirm("Bạn chắc chắn muốn xóa !");
                    if (r == true) {
                        $.ajax({
                            type: "POST",
                            data: {
                                _method: 'delete'
                            },
                            url: route('reg.destroy', {
                                id: page_id
                            }),
                            success: function (data) {
                                table.draw();
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });
                    } else {
                        return false;
                    }

                });
                $(".alert").fadeTo(2000, 500).slideUp(500, function () {
                    $(".alert").slideUp(500);
                });

            });
        </script>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/reg/index.blade.php ENDPATH**/ ?>