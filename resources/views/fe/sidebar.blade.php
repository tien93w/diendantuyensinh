<div class="page-siderbar  boder-solid-topsirderbar plr-10">
   <h2 class="page-siderbar-title">THÔNG TIN TUYỂN SINH</h2>

   <div class="row m-0 ">
      <ul class="pl-0">
         <?php //dd($tuyensinh);?>
         @foreach ($tuyensinh as $item)
            <li class="siderbar-item-child">
               <div class="col-md-4 p-0">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                     <img src="{{ $item->image}}" class="img-fluid" alt="">
                  </a>
               </div>
               <div class="col-md-8 pr-0">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                     <h2>{{ $item->title}}</h2>
                  </a>
               </div>
            </li>
         @endforeach
      </ul>
   </div>
</div>


<div class="page-siderbar-2 pt-3 mt-3 mb-3 boder-solid-topsirderbar plr-10">
   <h2 class="page-siderbar-title">CÁC TRƯỜNG LIÊN THÔNG</h2>

   <div class="row m-0 ">
      <ul class="pl-0" style="    display: contents;">
         @foreach ($lienthong as $item)
         <li class="pt-1 col-4 p-0 siderbar-item-child-2">
               <div class="col-md-12 p-0">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                  <img src="{{ $item->image}}" class="img-fluid" alt="">
                  </a>
               </div>
               <div class="col-md-12 p-0">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                  <h2>{!! substr($item->title,
                                        0, 58)!!}</h2>
               </a>
               </div>

         </li>
         @endforeach

      </ul>
   </div>
</div>

<div class="page-ads-img  sticky-top-ads">
   <div class="">
      <div class="row m-0">
<!-- diendantuyensinh24h.com - 300x600 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-1895892504965300"
     data-ad-slot="3410095460"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
      </div>
   </div>

</div>
<script>

</script>