<?php ?>


<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thông tin học viên</div>

                <div class="panel-body">


                    <div class="form-group">
                        <label for="title" class="col-md-4 control-label">Tên học viên</label>
                        <?php echo e($data->student); ?>

                    </div>


                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Ngày sinh</label>
                        <?php echo e($data->date); ?>

                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Số điện thoại phụ huynh</label>
                        <?php echo e($data->parent_phone); ?>

                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Tên phụ huynh</label>
                        <?php echo e($data->parent_name); ?>

                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Số điện thoại</label>
                        <?php echo e($data->phone); ?>

                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Địa chỉ</label>
                        <?php echo e($data->address); ?>

                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Hệ xét tuyển</label>
                        <?php echo e($data->addmission); ?>

                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Facebook</label>
                        <a href="<?php echo e($data->facebook); ?>" target="_blank"><?php echo e($data->facebook); ?></a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/reg/show.blade.php ENDPATH**/ ?>