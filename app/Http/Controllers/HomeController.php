<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategoryPost;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\ContactMobileRequest;
use App\Models\Slug;
use Mail;
use Illuminate\Support\Str;
use App\Models\Page;
use App\Models\Slide;
use App\Models\Tag;
use App\Models\Reg;
use Carbon\Carbon;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Slug\SlugRepositoryInterface;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Post\PostRepositoryInterface;
use App\Models\Contact;
use Illuminate\Support\Facades\Auth;
use Corcel\Model\Taxonomy;
// use Corcel\Model\Post;
use App\Models\Post;
use App\Http\Requests\RegRequest;

class HomeController extends Controller
{

  protected $cateRepository;
  protected $repoSlug;
  protected $repoPage;
  protected $repoPost;
  public function __construct(
  CategoryRepositoryInterface $cateRepository,
  SlugRepositoryInterface $repoSlug,
  PageRepositoryInterface $repoPage,
  PostRepositoryInterface $repoPost
  )
  {
      $this->cateRepository = $cateRepository;
      $this->repoSlug = $repoSlug;
      $this->repoPage = $repoPage;
      $this->repoPost = $repoPost;
  }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $now=date("Y-m-d");
        $cat = $this->repoPost->limit(11)->orderBy('id','DESC')->with('slugs')->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->get();
        $tinlienthong = Post::whereHas('cates', function ($query){
          $query->where('cate_id',29);
        })->with('slugs')->orderBy('id', 'desc')->limit(4)->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->get();
        $diemchuandh = Post::whereHas('cates', function ($query){
          $query->where('cate_id',24);
        })->with('slugs')->orderBy('id', 'desc')->limit(8)->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->get();
        $tsdh = CategoryPost::select()->where('pid',2)->with('slugs')->get();
        $tscd = CategoryPost::select()->where('pid',3)->with('slugs')->get();
        $tstc = CategoryPost::select()->where('pid',4)->with('slugs')->get();
        $tingd = Post::whereHas('cates', function ($query){
          $query->where('cate_id',26);
        })->with('slugs')->orderBy('id', 'desc')->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->limit(4)->get();
        $thpt = Post::whereHas('cates', function ($query){
          $query->where('cate_id',7);
        })->with('slugs')->orderBy('id', 'desc')->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->limit(4)->get();
        $chungchi = Post::whereHas('cates', function ($query){
          $query->where('cate_id',28);
        })->with('slugs')->orderBy('id', 'desc')->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->where('public',1)->limit(4)->get();
        $duhoc = Post::whereHas('cates', function ($query){
          $query->where('cate_id',25);
        })->with('slugs')->orderBy('id', 'desc')->limit(3)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->get();
        $khoithi = Post::whereHas('cates', function ($query){
          $query->where('cate_id',27);
        })->with('slugs')->orderBy('id', 'desc')->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->limit(5)->get();
        $dkdiemchuan = Post::whereHas('cates', function ($query){
          $query->where('cate_id',31);
        })->with('slugs')->orderBy('id', 'desc')->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->limit(8)->get();
        $truonglt = Post::whereHas('cates', function ($query){
          $query->where('cate_id',5);
        })->with('slugs')->orderBy('id', 'desc')->where('public',1)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->limit(12)->get();
        $catediemchuan = CategoryPost::select()->where('pid',24)->with('slugs')->get();
        $data = [
          'postnews'=>$cat,
          'tinlienthong'=>$tinlienthong,
          'diemchuandh'=>$diemchuandh,
          'tsdh'=>$tsdh,
          'tscd'=>$tscd,
          'tstc'=>$tstc,
          'tingd'=>$tingd,
          'thpt'=>$thpt,
          'chungchi'=>$chungchi,
          'duhoc'=>$duhoc,
          'khoithi'=>$khoithi,
          'catediemchuan'=>$catediemchuan,
          'dkdiemchuan'=>$dkdiemchuan,
          'truonglt'=>$truonglt
        ];
        return view('home',$data);
    }

    public function allslug($slug){
      \Shortcode::enable();
        $dataslug = Slug::select()->where('slug', $slug)->first();
        if(!$dataslug){
            return redirect()->route('home');
        }
        if($dataslug->type =="cate"){
            if($dataslug->slug == 'lien-thong-dai-hoc'){
            $data = $this->repoPage->where('id',1)->first();
            $datapc = CategoryPost::select()->where('slug_id', $dataslug->id)->first();
            $cateId =$datapc->id;
            $datas = Post::whereHas('cates', function ($query) use($cateId){
              $query->where('cate_id',$cateId);
            })->with('slugs')->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->orderBy('id', 'desc')->get();
            $datanews = Post::select()->orderBy('id', 'desc')->limit(5)->get();
            return view('fe.pagelienthong', compact('data','datas','datapc','datanews'));
          }
          $datapc = CategoryPost::select()->where('slug_id', $dataslug->id)->first();
          $cateId =$datapc->id;
          $datas = Post::whereHas('cates', function ($query) use($cateId){
            $query->where('cate_id',$cateId);
          })->with('slugs')->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->orderBy('id', 'desc')->paginate(15);
          $datanews = Post::select()->orderBy('id', 'desc')->limit(5)->get();
          return view('fe.catepost',compact('datas','datanews','datapc'))->withShortcodes();
        }

        if($dataslug->type =="new"){
          $data = $this->repoPost->with('catesview')->where('slug_id', $dataslug->id)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->first();
          if ($data){
          $data->setRelation('comments', $data->comments()->paginate(5));
          $cateId = $data->catesview->pluck('id');
          $cate = CategoryPost::where('id',$cateId)->with('slugs')->first();
          $datanews = Post::whereHas('cates', function ($query) use ($cateId){
            $query->where('cate_id',$cateId);
          })->with('slugs','catesview')->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->inRandomOrder()->limit(10)->get();
          return view('fe.post',compact('data','datanews','cate'))->withShortcodes();;

          }else{
              return redirect()->route('home');
          }
        }
        if($dataslug->type =="newcustom"){
          $data = $this->repoPost->with('catesview')->where('slug_id', $dataslug->id)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->first();
          if ($data){
          $data->setRelation('comments', $data->comments()->paginate(5));
          $cateId = $data->catesview->pluck('id');
          $cate = CategoryPost::where('id',$cateId)->with('slugs')->first();
          $datanews = Post::whereHas('cates', function ($query) use ($cateId){
            $query->where('cate_id',$cateId);
          })->with('slugs','catesview')->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->inRandomOrder()->limit(10)->get();
            return view('fe.postcustom',compact('data','datanews','cate'))->withShortcodes();
          }else{
              return redirect()->route('home');
          }
        }
        if($dataslug->type =="landing"){
          $data = Post::select()->where('slug_id', $dataslug->id)->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->first();
          if ($data){
          $data->setRelation('comments', $data->comments()->paginate(5));
          $datanews = Post::select()->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->orderBy('id', 'desc')->limit(5)->get();
          return view('fe.landing',compact('data','datanews'))->withShortcodes();
          }else{
              return redirect()->route('home');
          }
        }

        if($dataslug->type =="page"){
          $data = Page::select()->where('slug_id', $dataslug->id)->first();
          $datanews = Post::select()->whereNull('published_at')->orWhere('published_at', '<=', Carbon::now())->orderBy('id', 'desc')->limit(10)->get();
          return view('fe.page',compact('data','datanews'))->withStripShortcodes();
        }
    }
    public function tag($slug){

      $datas = Post::whereHas('tag', function($q) use ($slug)
      {
          $q->where('slug', $slug);
      })->paginate(10);
      $datapc = Tag::select()->where('slug',$slug)->first();
      return view('fe.tagpost',compact('datas','datapc'));
    }

    public function contact(ContactRequest $request ){

        $data['title'] = $request->student;
        $data['phone'] = $request->phone;
        $data['admission'] = $request->addmission;
        $mail = Mail::send('emails.contact', ['data'=>$data], function($message) {

          $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

          ->subject('Trường cao đẳng nấu ăn Hà Nội');
        });

                Reg::create($request->all());
            return redirect()->back()
            ->with('success','Thông tin được gửi thành công. Chúng tôi sẽ liên hệ lại trong thời gian sớm nhất.');


    }
    public function contactm(ContactMobileRequest $request ){

      $data['title'] = $request->student;
      $data['phone'] = $request->phone;
      $data['admission'] = $request->addmission;
      $mail = Mail::send('emails.contact', $data, function($message) {

        $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

        ->subject('Trường cao đẳng nấu ăn Hà Nội');
      });

          Reg::create($request->all());
          return redirect()->back()
          ->with('success','Thông tin được gửi thành công. Chúng tôi sẽ liên hệ lại trong thời gian sớm nhất.');


  }
    public function xettuyen(RegRequest $request){
        Reg::create($request->all());
        $data['title'] = $request->student;
        $data['phone'] = $request->phone;
        $data['admission'] = $request->addmission;
        $mail = Mail::send('emails.contact', ['data'=>$data], function($message) {

          $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

          ->subject('Trường cao đẳng nấu ăn Hà Nội');
        });
        return redirect()->back()
            ->with('success','Đăng Ký Thành Công');
    }
}
