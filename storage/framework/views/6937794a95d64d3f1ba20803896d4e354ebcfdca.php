<?php $__env->startSection('content'); ?>

<div class="container">
    <form action="<?php echo e(route('site.store')); ?>" method="POST">
        <?php echo e(csrf_field()); ?>


        <div class="form-group">
            <label for="" class="control-label">Tiêu đề trang</label>
            <input type="text" class="form-control" name="title"
                value="<?php echo e(isset($datas) ? $datas->title :null); ?>">
        </div>
        <div class="form-group">
            <label for="" class="control-label">Mô tả trang</label>
            <textarea name="discription" id="" class="form-control"
                rows="4"><?php echo isset($datas) ? $datas->discription :null; ?></textarea>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Từ khoá trang</label>
            <textarea name="keysword" id="" class="form-control"
                rows="4"><?php echo isset($datas) ? $datas->keysword :null; ?></textarea>
        </div>
        
        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button type="submit" class="btn btn-primary">
                    Lưu lại
                </button>

                <a class="btn btn-link" href="<?php echo e(route('site.create')); ?>">
                    Hủy
                </a>
            </div>
        </div>
    </form>
</div>
<script src="http://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>


<script src="<?php echo e(asset('/vendor/laravel-filemanager/js/stand-alone-button.js')); ?>"></script>
<script type="text/javascript">
    $('#lfm').filemanager('image');
    $('#favicon').filemanager('image');
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/config/index.blade.php ENDPATH**/ ?>