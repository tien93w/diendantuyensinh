<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title')->default('');
            $table->string('image')->nullable();
            $table->bigInteger('slug_id')->unsigned();
            $table->foreign('slug_id')->references('id')->on('slugs')->onDelete('cascade');
            $table->bigInteger('cid')->nullable();
            $table->bigInteger('uid')->nullable();
            $table->text('description')->nullbale();
            $table->longText('content');
            $table->boolean('feature')->default(0);
            $table->boolean('public')->default(1);
            $table->integer('viewcount')->default(0);
            $table->text('meta_title')->nullable();
            $table->text('keywords')->nullable();
            $table->text('mdescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
