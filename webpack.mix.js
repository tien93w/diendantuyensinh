const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'public/fe/css/bootstrap.min.css',
    'public/fe/css/style.css',
    'public/fe/css/font-awesome.min.css',
    'public/fe/css/reponsive.css'
 ], 'public/css/app.css');
 mix.scripts([
    'public/fe/js/jquery.js',
    'public/fe/js/bootstrap.min.js'
], 'public/js/app.js');
