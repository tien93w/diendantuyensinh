<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomCate extends Model
{
    protected $fillable = ['description','content','cate_id'];
}
