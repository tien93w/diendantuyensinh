// $(document).ready(function () {
//     $('.navbar-light .dmenu').hover(function () {
//             $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
//         }, function () {
//             $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
//         });
//     });

//banner ads siderbar
$(window).scroll(function (e) {
    var $el = $('.sticky-top-ads');
    var isPositionFixed = ($el.css('position') == 'fixed');
    if ($(this).scrollTop() > 500 && !isPositionFixed) {
        $el.css({
            'position': 'sticky',
            'top': '10px'
        });
    }
    if ($(this).scrollTop() < 500 && isPositionFixed) {
        $el.css({
            'position': 'static',
            'top': '10px'
        });
    }
});
// social
$(window).scroll(function (e) {
    var $el = $('.social-left-child');
    var isPositionFixed = ($el.css('position') == 'fixed');
    if ($(this).scrollTop() > 350 && !isPositionFixed) {
        $el.css({
            'position': 'sticky',
            'top': '10px'
        });
    }
    if ($(this).scrollTop() < 350 && isPositionFixed) {
        $el.css({
            'position': 'static',
            'top': '10px'
        });
    }
});