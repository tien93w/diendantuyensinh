<?php ?>



<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Sửa bài viết
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="<?php echo e(route('allslug', $data2->slugs->slug)); ?>"
                            target="_blank">XEM TRƯỚC</a>
                    </li>
                </ul>
            </div>

        </div>
        <div class="m-portlet__body">

            <!-- Display Validation Errors -->
            <?php echo $__env->make('admin.errors', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


            <form class="form-horizontal" style="width:100%" role="form" method="POST"
                action="<?php echo e(route('page.update',$data2->id)); ?>">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('PATCH')); ?>


                <div class="row">
                    <div class="col-md-9">
                        <div
                            class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                            <label for="title" class="col-md-12 control-label">Tên trang</label>

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title"
                                    value="<?php echo e($data2->title); ?>" required autofocus>

                                <?php if($errors->has('title')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>




                        <div
                            class="form-group<?php echo e($errors->has('slug') ? ' has-error' : ''); ?>">
                            <label for="slug" class="col-md-12 control-label">Slug bài viết</label>

                            <div class="col-md-12">
                                <input id="slug" type="text" class="form-control" name="slug"
                                    value="<?php echo e($data2->slugs->slug); ?>">

                                <?php if($errors->has('slug')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('slug')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div
                            class="form-group<?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-12 control-label">Mô tả ngắn</label>

                            <div class="col-md-12">
                                <textarea id="description" rows="7" name="description"
                                    class="form-control my-editor"><?php echo e($data2->description); ?></textarea>
                                <?php if($errors->has('description')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('description')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div
                            class="form-group<?php echo e($errors->has('content') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-12 control-label">Mô tả </label>

                            <div class="col-md-12">
                                <textarea id="content" rows="40" name="content"
                                    class="form-control my-editor"><?php echo e($data2->content); ?></textarea>
                                <?php if($errors->has('content')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('content')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-top: 30px;padding-bottom: 30px">
                            <h2>SEO
                                <span style="font-size: 40px; float: right" class="" data-toggle="collapse"
                                    data-target="#demo"><i class="fa fa-angle-double-down"
                                        style="font-size: 40px;color: #0b51c5" aria-hidden="true"></i></span>
                            </h2>
                        </div>
                        <div id="demo" class="collapse">
                            <div
                                class="form-group<?php echo e($errors->has('meta_title') ? ' has-error' : ''); ?>">
                                <label for="meta_title" class="col-md-12 control-label">Meta Title</label>

                                <div class="col-md-12">
                                    <div id="ameta_title"></div>
                                    <input rows="7" id="meta_title" name="meta_title" class="form-control"
                                        value="<?php echo e($data2->meta_title); ?>"></input>
                                    <?php if($errors->has('meta_title')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('meta_title')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div
                                class="form-group<?php echo e($errors->has('keywords') ? ' has-error' : ''); ?>">
                                <label for="keywords" class="col-md-12 control-label">Meta Keywords</label>

                                <div class="col-md-12">
                                    <div id="akeywords"></div>
                                    <input rows="7" id="keywords" name="keywords" class="form-control"
                                        value="<?php echo e($data2->keywords); ?>"></input>
                                    <?php if($errors->has('keywords')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('keywords')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div
                                class="form-group<?php echo e($errors->has('mdescription') ? ' has-error' : ''); ?>">
                                <label for="mdescription" class="col-md-12 control-label">Meta Description</label>

                                <div class="col-md-12">
                                    <div id="amdescription"></div>
                                    <textarea rows="4" id="mdescription" name="mdescription"
                                        class="form-control"><?php echo e($data2->mdescription); ?></textarea>
                                    <?php if($errors->has('mdescription')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('mdescription')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">
                            

                        </div>
                        <div class="col-md-12">
                            <label for="image" class="control-label">Ảnh đại diện</label><br>
                            <span class="form-group-btn">
                                <a id="lfm" data-input="image" data-preview="holder" class="btn btn-primary text-white">
                                    <i class="fa fa-picture-o"></i> Chọn
                                </a>
                            </span>
                            <input id="image" class="form-control col-md-12" type="text" name="image"
                                value="<?php echo e($data2->image); ?>">
                            <div id="holder" style="margin-top:15px;max-height:100px;">
                                <img src="<?php echo e(asset($data2->image)); ?>" alt="">
                            </div>
                        </div>

                    </div>
                </div>
        </div>






        <div class="form-group">
            <div class="col-md-8 col-md-offset-4" style="padding-bottom: 30px">
                <button type="submit" class="btn btn-primary">
                    Sửa
                </button>

                <a class="btn btn-link" href="<?php echo e(route('page.index')); ?>">
                    Hủy
                </a>
            </div>
        </div>
        </form>
    </div>
</div>
</div>


<script src="<?php echo e(asset('/vendor/laravel-filemanager/js/stand-alone-button.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.4.1/tinymce.min.js" integrity="sha512-c46AnRoKXNp7Sux2K56XDjljfI5Om/v1DvPt7iRaOEPU5X+KZt8cxzN3fFzemYC6WCZRhmpSlZvPA1pttfO9DQ==" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('/admin/js/tiny.js')); ?>"></script>
<script type="text/javascript">
    $('#lfm').filemanager('image');
</script>
<script>
    $('#meta_title').keyup(function () {
        $('#ameta_title').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
    $('#keywords').keyup(function () {
        $('#akeywords').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
    $('#mdescription').keyup(function () {
        $('#amdescription').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
</script>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/page/edit.blade.php ENDPATH**/ ?>