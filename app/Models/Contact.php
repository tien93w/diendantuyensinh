<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name','date','sex','phone','address','nameparent','point','phoneparent','admission','facebook'
    ];
}
