<?php ?>



<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Sửa Link trong bài viết
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>

        </div>
        <div class="m-portlet__body">

            <!-- Display Validation Errors -->
            <?php echo $__env->make('admin.errors', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <form class="form-horizontal" method="POST"
                action="<?php echo e(route('link.update', $data->id)); ?>">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('PATCH')); ?>


                <div
                        class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                    <label for="name" class="col-md-4 control-label">Tiêu đề link</label>

                    <div class="col-md-12">
                        <input id="title" type="text" class="form-control" name="title"
                               value="<?php echo e(old('title') ? old('title') : $data->title); ?>" required autofocus>
                    </div>
                </div>
                <div
                        class="form-group<?php echo e($errors->has('slug') ? ' has-error' : ''); ?>">
                    <label for="link" class="col-md-4 control-label">Link</label>

                    <div class="col-md-12">
                        <input id="link" type="text" class="form-control" name="link"
                               value="<?php echo e(old('link') ? old('link') : $data->link); ?>">
                    </div>
                </div>
                <div
                        class="form-group<?php echo e($errors->has('slug') ? ' has-error' : ''); ?>">
                    <label for="alt" class="col-md-4 control-label">Alt</label>

                    <div class="col-md-12">
                        <input id="alt" type="text" class="form-control" name="alt"
                               value="<?php echo e(old('alt') ?old('alt') : $data->alt); ?>">
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Sửa
                        </button>

                        <a class="btn btn-link" href="<?php echo e(route('link.index')); ?>">
                            Hủy
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/diendan/resources/views/admin/link/edit.blade.php ENDPATH**/ ?>