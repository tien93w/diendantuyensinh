jQuery(document).ready(function ($) {
    $(document).ready(function () {

        $('.related_list').slick({
            autoplay: true,
            infinite: true,
            speed: 800,
            slidesToShow: 4,
            slidesToScroll: 1,
            touchMove: false,
            appendArrows: '.related_nav',
            nextArrow: '<button class="btn_sl btn_sl-next"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn_sl btn_sl-pre"><i class="fa fa-angle-left"></i></button>',
            responsive: [{
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                }
            }]
        });


        // $("#sidebar_product").sticky({
        //     topSpacing: 0,
        //     bottomSpacing: 550
        // });
    });
});