<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title', 'image','slug_id','uid','description','content','public','viewcount','meta_title','keywords','mdescription','created_at','updated_at'];
    public function slugs()
    {
        return $this->hasOne('App\Models\Slug','id','slug_id');
    }

    public function users()
    {
        return $this->hasOne('App\Models\User','id','uid');
    }
}
