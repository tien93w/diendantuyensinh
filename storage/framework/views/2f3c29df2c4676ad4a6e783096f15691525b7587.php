<?php $__env->startSection('meta'); ?>
<?php if($meta): ?>
<title><?php echo e($meta->title ? $meta->title : "CAO ĐẲNG NẤU ĂN HÀ  NỘI"); ?></title>
<meta name="robots" content="noindex, nofollow" />
<meta property="og:image"           content="<?php echo e($meta->image != "" ? $meta->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png'); ?>" />
<meta name="keywords" content="<?php echo e($meta->keywords ? $meta->keywords : $meta->keysword); ?>"/>
<meta name="description" content="<?php echo e($meta->discription ? $meta->discription : ""); ?>"/>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('logo'); ?>
<h1>
    <a href="<?php echo e(route('home')); ?>">
        <img class="img-fluid" src="./images/logo.png" alt="">
    </a>
</h1>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- block one -->
<div class="container-fluid block-one">
    <div class="container">
        <div class="row">
            <div class="w-68">
                <div class="w-58 block-one-left">
                    <?php $__currentLoopData = $postnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($loop->first): ?>
                        <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                        <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                        </a>
                        <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                            <h2><?php echo e($item->title); ?></h2>
                        </a>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <div>
                        <ul>
                            <?php $__currentLoopData = $postnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($k <3 && $k >0): ?>
                            <li><a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>"><?php echo e($item->title); ?></a>
                            </li>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
                <div class="w-42 block-one-center">
                    <h2>TIN TỨC TUYỂN SINH</h2>
                    <ul>
                        <?php $__currentLoopData = $postnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($k > 2): ?>
                        <li>
                            <div class="col-md-12"  style="padding-left: 15px;
                            margin-top: -.5rem;">
                                <div class="row">
                            <div class="col-4 d-block d-sm-none">
                                <img class="img-fluid" src="<?php echo e($item->image); ?>" alt="<?php echo e($item->title); ?>"/>
                            </div>
                            <div class="col-8 col-md-12"><a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>"><?php echo e($item->title); ?></a><div>
                                </div>
                                </div>
                        </li>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
            <div class="w-32 block-one-right">
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
            </div>

        </div>
    </div>
</div>
<!-- end block one -->
<!-- block two -->
<div class="container block-two">

    <div class="row">
        <div class="w-68">
            <div class="mt-15">

                <ul class="nav nav-tab-home" role="tablist">
                    <li role="presentation" class="active"><a href="#diem-chuan" aria-controls="home" role="tab" data-toggle="tab">                <div class="row block-two-title">
                        <h2>Điểm chuẩn Đại học</h2>
                    </div></a></li>
                    <li role="presentation"><a href="#truong-lien-thong" aria-controls="profile" role="tab" data-toggle="tab">                 <div class="row block-two-title">
                        <span class="block-two-link">Dự kiến điểm chuẩn</span>
                    </div></a></li>
                    <li role="presentation" class=" btn-view-all-tab"><a href="<?php echo e(route('allslug','tin-tuc-lien-thong')); ?>" class="btn-view-all">xem toàn bộ<i
                        class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                  </ul>
            
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="diem-chuan">
                        <div class=" row  block-two-ct3 m-0">
                            <?php $__currentLoopData = $catediemchuan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($loop->last): ?>
                            <div class="col-md-6 pt-1 mobile-plr-10">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                            <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="<?php echo e($item->title); ?>">
                                </a>
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                    <h2><?php echo e($item->meta_title); ?></h2>
                                </a>
                                <p><?php echo e($item->description); ?></p>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6">
                                <div class="row">
                                    <ul class=" block-two-ct6-h2">
                                        <?php $__currentLoopData = $catediemchuan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($loop->first): ?>
                                        <?php endif; ?>
                                        <?php if($loop->remaining): ?>
                                        <li class="pt-1">
                                            <div class="col-md-4 p-0">
                                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                            <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="<?php echo e($item->title); ?>">
                                            </a>
                                            </div>
                                            <div class="col-md-8">
                                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                                <h2><?php echo e($item->meta_title); ?></h2>
                                            </a>
                                            </div>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
        
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="truong-lien-thong">
                        <div class="block-two-ct3">
                            <?php $__currentLoopData = $dkdiemchuan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class=" col-xs-12 col-md-3  col-sm-12">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                                </a>
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                <h2><?php echo e($item->title); ?></h2>
                                </a>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                  </div>

                <div class="clear"></div>
            </div>
            <div class="mt-15">

                <ul class="nav nav-tab-home" role="tablist">
                    <li role="presentation" class="active"><a href="#tin-tuc-lien-thong" aria-controls="home" role="tab" data-toggle="tab">                <div class="row block-two-title">
                        <h2>Tin tức Liên thông</h2>
                    </div></a></li>
                    <li role="presentation"><a href="#truong-lien-thong2" aria-controls="profile" role="tab" data-toggle="tab">                 <div class="row block-two-title">
                        <span class="block-two-link">Các Trường Liên
                            Thông</span>
                    </div></a></li>
                    <li role="presentation" class=" btn-view-all-tab"><a href="<?php echo e(route('allslug','tin-tuc-lien-thong')); ?>" class="btn-view-all">xem toàn bộ<i
                        class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                  </ul>
            
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tin-tuc-lien-thong">
                        <div class="block-two-ct3">
                            <?php $__currentLoopData = $tinlienthong; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class=" col-xs-12 col-md-3  col-sm-12">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                                </a>
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                <h2><?php echo e($item->title); ?></h2>
                                </a>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="truong-lien-thong2">
                        <div class="col-md-12 block-two-ct3 block-two-ct3-tab block-two-ct3-tab-mobile">
                            <?php $__currentLoopData = $truonglt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-4 col-xs-12 col-md-2  col-sm-12">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                                </a>
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                <h2><?php echo e($item->title); ?></h2>
                                </a>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                  </div>

                <div class="clear"></div>
            </div>

            <div class="mt-15">

                <ul class="nav nav-tab-home" role="tablist">
                    <li role="presentation" class="active"><a href="#khoi-thi" aria-controls="home" role="tab" data-toggle="tab">                <div class="row block-two-title">
                        <h2>Khối thi Đại học</h2>
                    </div></a></li>
                    <li role="presentation"><a href="#truong-lien-thong1" aria-controls="profile" role="tab" data-toggle="tab">                 <div class="row block-two-title">
                        <span class="block-two-link">Các Trường Liên
                            Thông</span>
                    </div></a></li>
                    <li role="presentation" class=" btn-view-all-tab"><a href="<?php echo e(route('allslug','khoi-thi')); ?>" class="btn-view-all">xem toàn bộ<i
                        class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                  </ul>
            
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="khoi-thi">
                        <div class="block-two-ct3">
                            <?php $__currentLoopData = $khoithi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($loop->last): ?>
                            <div class="col-md-6 pt-1 mobile-plr-10">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                            <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="<?php echo e($item->title); ?>">
                                </a>
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                    <h2><?php echo e($item->title); ?></h2>
                                </a>
                                <p><?php echo $item->description; ?></p>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-6">
                                <div class="row">
                                    <ul class=" block-two-ct6-h2">
                                        <?php $__currentLoopData = $khoithi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($loop->first): ?>
                                        <?php endif; ?>
                                        <?php if($loop->remaining): ?>
                                        <li class="pt-1">
                                            <div class="col-md-4 p-0">
                                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                            <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="<?php echo e($item->title); ?>">
                                            </a>
                                            </div>
                                            <div class="col-md-8">
                                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                                <h2><?php echo e($item->title); ?></h2>
                                            </a>
                                            </div>
                                        </li>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
        
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="truong-lien-thong1">
                        <div class="block-two-ct3">
                            <?php $__currentLoopData = $tinlienthong; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class=" col-xs-12 col-md-3  col-sm-12">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                                </a>
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                                <h2><?php echo e($item->title); ?></h2>
                                </a>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                  </div>

                <div class="clear"></div>
            </div>


        </div>
        <div class="w-32">
            <div class="siderbar-mobile">
            <div class="col-md-12 pt-md-3">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                    <h2 class="siderbar-1-title p-2">TUYỂN SINH ĐẠI HỌC</h2></span><span><a href="<?php echo e(route('allslug','dai-hoc')); ?>"
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="row m-0 ">
                            <ul class="pl-0">
                            <?php $__currentLoopData = $tsdh; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="">
                                <div class="col-md-4 p-0">
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                <img src="<?php echo e($item->image); ?>" class="img-fluid border-radius-5" alt="<?php echo e($item->title); ?>" max-height="69px">
                                    </a>
                                </div>
                                <div class="col-md-8 pr-0">
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                        <h2><?php echo e($item->meta_title); ?></h2>
                                    </a>
                                </div>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>

                    </div>

                </div>
            </div>
            </div>
            
        <div class="siderbar-mobile">
            <div class="col-md-12 pt-md-3">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                        <h2 class="siderbar-1-title p-2">TUYỂN SINH CAO ĐẲNG</h2></span><span><a href="<?php echo e(route('allslug','cao-dang')); ?>"
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="row siderbar-cd">
                                                <?php $__currentLoopData = $tscd; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4 col-4 siderbar-child">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                            <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="<?php echo e($item->title); ?>"  max-height="69px">
                                </a>
                                                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <h2><?php echo e($item->meta_title); ?></h2>
                                </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
            <div class="clear"></div>
            <div class="siderbar-mobile">
                        <div class="col-md-12 pt-md-3">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                    <h2 class="siderbar-1-title p-2">TUYỂN SINH TRUNG CẤP</h2></span><span><a href="<?php echo e(route('allslug','trung-cap')); ?>"
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="col-md-12 p-md-0">
                            <ul class="pl-0">
                            <?php $__currentLoopData = $tstc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="">
                                <div class="col-md-4 p-0">
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                <img src="<?php echo e($item->image); ?>" class="img-fluid border-radius-5" alt="<?php echo e($item->title); ?>" max-height="69px">
                                    </a>
                                </div>
                                <div class="col-md-8 pr-0">
                                <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                        <h2><?php echo e($item->meta_title); ?></h2>
                                    </a>
                                </div>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>

                    </div>

                </div>
            </div>
        </div> 


        </div>
    </div>
</div>
<!-- end block two -->
<!-- block three -->
<div class="container block-three">
    <div class="row">
        <div class="w-68">
            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Tin giáo dục</h2><span><a href="<?php echo e(route('allslug','giao-duc-dao-tao')); ?>" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0 pt-md-2">
                    <?php $__currentLoopData = $tingd; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($loop->first): ?>
                        <div class="col-md-3">
                        <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                        <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                <h2 class="p-0 block-three-title-child"><?php echo e($item->title); ?></h2>
                            </a>
                            <p><?php echo $item->description; ?></p>
                        <?php endif; ?>
                        <ul class="pl-3 mb-0">
                        <?php if($loop->remaining): ?>
                            <li><a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <?php echo e($item->title); ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Thi THPT Vào 10</h2><span><a href="<?php echo e(route('allslug','tin-tuc-thpt')); ?>" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0  pt-md-2">
                    <?php $__currentLoopData = $thpt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($loop->first): ?>
                        <div class="col-md-3">
                        <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                        <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                <h2 class="p-0 block-three-title-child"><?php echo e($item->title); ?></h2>
                            </a>
                            <p><?php echo $item->description; ?></p>
                        <?php endif; ?>
                        <ul class="pl-3 mb-0">
                        <?php if($loop->remaining): ?>
                            <li><a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <?php echo e($item->title); ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Chứng chỉ</h2><span><a href="<?php echo e(route('allslug','chung-chi')); ?>" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0  pt-md-2">
                    <?php $__currentLoopData = $chungchi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($loop->first): ?>
                        <div class="col-md-3">
                        <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                        <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="col-md-9">
                            <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                <h2 class="p-0 block-three-title-child"><?php echo e($item->title); ?></h2>
                            </a>
                            <p><?php echo $item->description; ?></p>
                        <?php endif; ?>
                        <ul class="pl-3 mb-0">
                        <?php if($loop->remaining): ?>
                            <li><a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                                    <?php echo e($item->title); ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="mt-15">
                <div class="row block-two-title">
                <h2>Lao động - Du học</h2> <span class="block-two-link"></span><span><a href="<?php echo e(route('allslug','lao-dong-du-hoc')); ?>" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3">
                    <?php $__currentLoopData = $duhoc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class=" col-xs-12 col-md-3  col-sm-12">
                    <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>">
                    <img src="<?php echo e($item->image); ?>" class="img-fluid" alt="">
                        </a>
                        <a href="<?php echo e(route('allslug',$item->slugs->slug)); ?>" class="block-two-ct3-h2">
                            <h2><?php echo e($item->title); ?></h2>
                        </a>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="w-32 block-three-right">
            <div class="col-md-12">

<!-- diendantuyensinh24h.com - 336x280 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-1895892504965300"
     data-ad-slot="6404403391"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

            </div>
            <div class="col-md-12 pt-2">
<!-- diendantuyensinh24h.com - 300x600 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-1895892504965300"
     data-ad-slot="3410095460"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
        </div>
    </div>
</div>
<!-- end block three -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.fe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/httpsbao/tranhnhadep.net/resources/views/home.blade.php ENDPATH**/ ?>